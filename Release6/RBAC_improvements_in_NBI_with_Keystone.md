# RBAC improvements in NBI with Keystone #
 
## Proposer ##
- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)
- Eduardo Sousa (IT-Aveiro)
- Diogo Gomes (IT-Aveiro)
 
## Type ##
**Feature**
 
## Target MDG/TF ##
NBI, OSM_client
 
## Description ##
The NBI module could benefit immediately of the advantages that Keystone would 
provide to support and extend its RBAC functionality. Among other aspects, the 
integration of Keystone would be expected to provide:
- Out of the box support of different backends for user authentication: LDAP, 
Kerberos, etc.
- Would provide a simpler mean to configure, maintain and evolve the different 
types of roles in OSM.
- Would open the path for internal modules to leverage on its token mechanisms 
to enable or disable selectively some functionalities for a given user request.

## Demo or definition of done ##
- Authenticate NBI users against an external user directory, such as LDAP.
- Configure some roles and check that they are applied accordingly (as 
described in the corresponding Rel THREE feature).
