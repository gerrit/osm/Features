# Disable Port Security at Network Level #

## Proposer ##
- Syed Anwar (VMware)
- Ravindran Ganapathi (VMware)

## Type ##
**Feature**

## Target MDG/TF ##
IM,RO

## Description ##

We have found that with VMware Integrated Openstack 5.1 + NSX T + ENS current OSM deployment
fails due to port security not supported by ENS.

Failure point in OSM is when creating the network on openstack,
we observed that manual network creation in openstack also fails with error
'Port security is not supported on ENS Transport zones'

However we were able to create network manually in openstack using --disable-port-security
option while creating the network.
But we do not have any option in OSM IM to specify disable port security at the network level.

From the openstack documentation we can see that by default when the network is created
port security is always enabled,

--enable-port-security
Enable port security by default for ports created on this network (default)

--disable-port-security
Disable port security by default for ports created on this network


From OSM in the current vnfd IM we can find that we have the option to disable port security
but that works only at the connection point / port level.

Also in OSM there is an advanced configuration parameter that can be passed while registering the VIM in OSM,
i.e. no_port_security_extension but even that is applied only when creating ports and not networks.

There needs to be a mechanism using which we can disable port security by default for ports
created on a networks for openstack deployments using OSM.

## Design ##
Design pad: https://osm.etsi.org/pad/p/feature7326

## Demo or definition of done ##

We should be able to instantiate a NS via OSM by specifying the option to disable port security
while network creation on an VMware Integrated Openstack VIM which is configured with NSX T + ENS.
