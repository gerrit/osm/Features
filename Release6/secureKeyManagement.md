# Secure Key Management

## Proposer ##
Michael Marchetti

## Type ##
**Feature**

## Target MDG/TF ##
SO,RO,VCA

## Description ##

Please refer to this presentation:
https://docbox.etsi.org/OSG/OSM/05-CONTRIBUTIONS/2017
//OSM(17)000023_ssh_key_management_in_osm_release_1.pptx

This feature relates to the usage of proxy charms and allowing a proxy charm (charm container)
ssh access to a VNF virtual machine.  As outlined in the presentation, the proxy charm container
needs a private key that matches the injected public key in the VNF virtual machine.

This feature highlights some questions:
- should the private key be injected into the container or be self generated?
- how does the associated public key get injected into the VNF VM?
- all aspects of the key methodology should be transparent to the user
  (user should not have to manually add keys to the system).

It is expected that interactions between SO, RO and VCA will be necessary in order order to
orchestratrate the keys between the proxy charm and VNF.

Also of concern is accessibility to the ssh private key.  Some consideration should be made for who
has access to the private key.  When a user runs "juju config 'application'" inside the VCA or via
REST, should the private keys be visible?

## Demo or definition of done ##

A test case involves a VNF VM with a proxy charm,  utilzing the sshproxy charm.  The proxy charm
is invoked on configuration changes and performs an ssh remote session to the VNF VM.  The
private/public key between the proxy charm container and the VNF VM would have be automatically
deployed allowing this ssh remote session (without passwords) to commence.
