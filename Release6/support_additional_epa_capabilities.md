# Support for additional EPA capabilities #

## Proposer ##
- Syed Anwar (VMware)
- Ravindran Ganapathi (VMware)

## Type ##
**Feature**

## Target MDG/TF ##
IM,RO

## Description ##

Goal of the feature is to enable additional EPA capibilities for at VDU level related to:

- Resource Allocation
- Resource Placement

------------------------------------------------------------------------------------------------


More description on Resource Allocation primitives:

Resource Allocation Limit
-------------------------

Limit specifies an upper bound for CPU, memory, or storage I/O resources that can be allocated to a virtual machine.
A server can allocate more than the reservation to a virtual machine, but never allocates more than the limit, even if there are unused resources on the system.
The limit is expressed in concrete units (megahertzǰ megabytes, or I/O operations per second). CPU, memory, and storage I/O resource limits default to unlimited.

When the memory limit is unlimited, the amount of memory configured for the virtual machine when it was created becomes its effective limit.
In most cases, it is not necessary to specify a limit.

There are benefits and drawbacks:

    Benefits
    Assigning a limit is useful if you start with a small number of virtual machines and want to manage user expectations.
    Performance deteriorates as you add more virtual machines. You can simulate having fewer resources available by specifying a limit.

    Drawbacks
    You might waste idle resources if you specify a limit.
    The system does not allow virtual machines to use more resources than the limit, even when the system is underutilized and idle resources are available.
    Specify the limit only if you have good reasons for doing so.


Resource Allocation Reservation
-------------------------------

A reservation specifies the guaranteed minimum allocation for a virtual machine.

vCenter Server or ESXi allows you to power on a virtual machine only if there are enough unreserved resources to satisfy the reservation of the virtual machine.

The server guarantees that amount even when the physical server is heavily loaded.
The reservation is expressed in concrete units (megahertz or megabytes).

For example, assume you have 2GHz available and specify a reservation of 1GHz for VM1 and 1GHz for VM2.
Now each virtual machine is guaranteed to get 1GHz if it needs it. However, if VM1 is using only 500MHz, VM2 can use 1.5GHz.

Reservation defaults to 0.
You can specify a reservation if you need to guarantee that the minimum required amounts of CPU or memory are always available for the virtual machine.


Resource Allocation Shares
-------------------------

Shares specify the relative importance of a virtual machine (or resource pool).

If a virtual machine has twice as many shares of a resource as another virtual machine,
it is entitled to consume twice as much of that resource when these two virtual machines are competing for resources.

You can assign a specific number of shares (which expresses a proportional weight) to each virtual machine.
Specifying shares makes sense only with regard to sibling virtual machines or resource pools, that is,
virtual machines or resource pools with the same parent in the resource pool hierarchy.

Siblings share resources according to their relative share values, bounded by the reservation and limit.

When you assign shares to a virtual machine,
you always specify the priority for that virtual machine relative to other powered-on virtual machines.


For more references
-------------------

    https://docs.vmware.com/en/VMware-vSphere/6.5/vsphere-esxi-vcenter-server-65-resource-management-guide.pdf
    https://docs.openstack.org/nova/queens/user/flavors.html
    https://docs.vmware.com/en/VMware-Integrated-OpenStack/5.0/com.vmware.openstack.admin.doc/GUID-286DAE9A-A267-4A87-8636-AAC465A071DC.html

------------------------------------------------------------------------------------------------


Properties to support related to Resource Allocation
------------------------------------------------

CPU Allocation:

    -----------------------------

    quota:cpu_limit

    Description   :
    - Specify the maximum CPU allocation in MHz. The value 0 indicates that CPU usage is not limited.
    - This parameter ensures that the instance never uses more than the defined amount of CPU allocation.

    -----------------------------

    quota:cpu_reservation

    Description   :
    - Specifies the guaranteed minimum CPU reservation in MHz.
    - This means that if needed, the machine will definitely get allocated the reserved amount of CPU cycles.

    -----------------------------

    quota:cpu_shares_share

    Description   :
    - Specifies the number of CPU shares allocated.
    - Specifies the proportional weighted share for the domain. If this element is omitted, the service defaults to the OS provided defaults.
    - There is no unit for the value; it is a relative measure based on the setting of other VMs.
    - For example, a VM configured with value 2048 gets twice as much CPU time as a VM configured with value 1024.
    - When you assign CPU shares to a project, your primary concern is not the number of shares the project has.
      Knowing how many shares the project has in comparison with other projects is more important.

    -----------------------------


Memory Allocation:

    -----------------------------

    quota:memory_limit

    Description   :
    - Specify the maximum memory allocation in MB. The value 0 indicates that memory usage is not limited.
    - The memory utilization of a virtual machine will not exceed this limit, even if there are available resources.
    - This is typically used to ensure a consistent performance of virtual machines independent of available resources.

    -----------------------------

    quota:memory_reservation

    Description   :
    - Specifies the guaranteed minimum memory reservation in MB .
    - This means the specified amount of memory will definitely be allocated to the machine.

    -----------------------------

    quota:memory_shares_share

    Description   :
    - Specifies the number of memory shares allocated.
    - When there is resource contention, this value is used to determine the resource allocation.

    -----------------------------


Disk I/O Allocation:

    -----------------------------

    quota:disk_io_limit

    Description   :
    - Specify the maximum disk transaction allocation in IOPS. The value 0 indicates that disk transactions are not limited.
    - Specifies the upper limit for disk utilization in I/O per second.
    - The utilization of a virtual machine will not exceed this limit, even if there are available resources.

    -----------------------------

    quota:disk_io_reservation

    Description   :
    - Specifies the guaranteed minimum disk allocation in terms of Input/output Operations Per Second (IOPS).

    -----------------------------

    quota:disk_io_shares_share

    Description   :
    - Specifies the number of disk transaction shares allocated.
    - When there is resource contention, this value is used to determine the resource allocation.

    -----------------------------


Bandwidth Alocation:

    -----------------------------

    quota:vif_limit

    Description   :
    - Specify the maximum virtual interface bandwidth allocation in Mbps. The value 0 indicates that virtual interface bandwidth is not limited.
    - This parameter ensures that the VIF never uses more than the defined amount of bandwidth.

    -----------------------------

    quota:vif_reservation

    Description   :
    - Specify the guaranteed virtual interface bandwidth allocation in Mbps.
    - This parameter ensures that the virtual adapter on the instance gets the reserved amount of bandwidth during resource contention.
    - If the instance uses less than the reserved amount, the remainder is available to other virtual adapters.

    -----------------------------

    quota:vif_shares_share

    Description   :
    - Specifies the number of virtual interface bandwidth shares allocated.
    - When there is resource contention, this value is used to determine the resource allocation.

    -----------------------------


-------------------------------------------------------------------------------------------------


Properties to support related to Resource Placement
---------------------------------------------------

    vmware:hw_version

    Description   :
    - Specify the hardware version used to create images.
    - In an environment with different host versions, you can use this key to place instances on the correct hosts.
    - Example of hardware version: hw14

  -----------------------------

    vmware:storage_policy

    Description   :
    - Virtual machine storage policies are essential to virtual machine provisioning.
    - The policies control which type of storage is provided for the virtual machine, how the virtual machine is placed within the storage, and which data services are offered for the virtual machine.
    - vSphere includes default storage policies. However, you can define and assign new policies. You use the VM Storage Policies interface to create a storage policy.
    - When you define the policy, you specify various storage requirements for applications that run on virtual machines.
    - You can also use storage policies to request specific data services, such as caching or replication, for virtual disks.
    - You apply the storage policy when you create, clone, or migrate the virtual machine.
    - After you apply the storage policy, the Storage Policy Based Management (SPBM) mechanism places the virtual machine in a matching datastore and, in certain storage environments,
      determines how the virtual machine storage objects are provisioned and allocated within the storage resource to guarantee the required level of service.
    - The SPBM also enables requested data services for the virtual machine. vCenter Server monitors policy compliance and sends an alert if the virtual machine is in breach of the assigned storage policy.

    -----------------------------

## Design ##

Design pad: https://osm.etsi.org/pad/p/feature7106

## Demo or definition of done ##

We should be able to define the cpu/memory/disk-iops/vif-bandwidth allocation parameters in the vnfd IM descriptior.

While instantianiting the NS package for Openstack the above properties should be processed by the openstack connector,
and set at flavor level as flavor extra specs for vm instantianation.