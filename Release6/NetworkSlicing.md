# Manage network slices #

## Proposer ##
- Diego López (Telefonica)
- Josep Martrat (ATOS)
- Ricard Vilalta (CTTC)

## Type ##
**Feature**

## Target MDG/TF ##
IM-NBI, SO/LCM, RO

## Description ##
A Network Slice Instance (NSI) is defined in NGMN white paper as a set of network functions and the resources for these network functions 
which are arranged and configured, forming a complete logical network, which meets certain characteristics in terms of available bandwidth, 
latency, or QoS, among others described in 5QI (5G QoS Indicator). 

3GPP has proposed a data model in Release 15, which consists of a list of Network Slice Subnetworks instances (NSSI). Each NSSI contains a 
set of network functions and the resources for these network functions that are arranged and configured to form a logical network. ETSI NFV 
and 5G Americas are the entities that directly map a NSI with NSs saying basically that the sub-network instances are the same as NSs.

The NSI contains NSSI, which in turn contain Network Functions (NFs) (e.g., belonging to Access Network (AN) and Core Network (CN)) as well 
as all information relevant to the interconnections between these NFs like topology of connections and individual link requirements (e.g. 
QoS attributes). The NSI is created by using a Network Slice Template (NST).

ETSI ISG NFV is actually highlighting the relationship between NSs and Slices / SubnetSlices in ETSI NFV EVE 012. This is important since 
the NFV MANO is familiar and supports the NS (and VNF) constructions. 

Our Slice Manager is an internal OSM component, which exposes an API aligned with 3GPP data model, and internally handles the slice 
lifecycle. Usage of one level of network service recursiveness is expected, as well as relationship with PNF will be required.

The feature thus encompases:
- Ofering a Network Slice Manager API
- Identifying in which parts of OSM the concept of service recursiveness is required.
- The lifecycle management of a slice including several NS that include VNFs and PNFs.

## Demo or definition of done ##
- Define a Network Slice Template
- Instantiate a network slice with a couple of network services (including VNF and PNF).
- NSI deployment uses recursivity.
