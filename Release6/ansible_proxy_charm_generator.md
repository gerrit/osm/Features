# Ansible Proxy Charm Generator  #

## Proposer ##
- Eduardo Sousa (Whitestack)
- Gianpietro Lavado (Whitestack)

## Type ##
**Feature**

## Target MDG/TF ##
VCA, Devops

## Description ##
The goal of this feature is to publish a generator for easily building a proxy charm capable of running multiple, parametrized Ansible playbooks over VNFs.

## Demo or definition of done ##
A proxy charm with multiple Ansible playbooks can be built and included in a VNF package with minimal manual intervention.