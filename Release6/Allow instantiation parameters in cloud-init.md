# Allow instantiation parameters in cloud-init

## Proposer

- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type

**Feature**

## Target MDG/TF

NBI, LCM, RO

## Description

OSM currently provides a mechanism to pass parameters when an instantiation (of 
NS or NSI) is triggered, so that different instances created out of the same 
NS/Slice Package may run with slightly different initial conditions -such as 
different IP addressing, different QoS definitions, more/less centralization, 
etc.-, which would be modelled as variables/inputs in the Package definition. 
Currently, two kinds of mechanisms (non-mutually exclusive) to consume those 
instantiation parameters are available:

1. Indications of VIM target for a given VNF. These are passed to RO to deploy 
the VNFs accordingly.
2. Initial configuration parameters. These are passed to VCA so that initial 
configuration can be influenced by these parameters.

This feature specifies a third mechanism of consumption of instantiation 
parameters, so that some parameters might be directly mapped to initial config 
files/templates, passed via cloud-init. This would largely simplify the initial 
parametrization of those VNFs where some parameters might be trivially mapped 
to fields in initial config templates. This would bring two benefits:

- It would make the onboarding and parametrization of some simple/static VNFs 
much simpler.
- Much faster instantiation of VNFs which required complex parametrizable 
configurations, avoiding the need of multiple/complex transactions.

This mechanism should be able to coexist the other two other mechanisms, even 
in the same VNFs, where different subsets of instantiation parameters at VNF 
level might trigger different types of operations.

## Demo or definition of done

- Create a VNF Package that embeds a config template with placeholders where 
some instantiation parameters might be directly mapped.
- Instantiate a NS that uses such a VNF, passing the appropriate instantiation 
parameters to be mapped to the template.
- Check that the applied configuration corresponds to the parameters that were 
passed via this mechanism.
- Check whether parameters marked as mandatory are not provided. If so, an 
error message should be issued, indicating clearly what parameter is needed in 
which VNF.
