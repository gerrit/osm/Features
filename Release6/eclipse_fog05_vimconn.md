# Eclipse fog05 vimconn #

## Proposer ##
Gabriele Baldoni, Angelo Corsaro (ADLINK Technology Inc.)

## Type ##
**Feature**

## Target MDG/TF ##
RO, NBI, UI

## Description ##
Adding the support for Eclipse fog05 as VIM, to leverage the possibility of running VNFs in a
Fog environment that can be packaged in differt ways other than VMs
(LXD Containers, Unikernels, binary applications...)

## Demo or definition of done ##
EU H2020 5GCity project will leverage on this for an UseCase where some VNFs need to be instantiated
in a lamp post that does not have support for VMs, in the specific usecase, VNFs will be packaged as
LXD containers and will be instantiated on low-end ARMv7 devices on the lamppost.
Part of the connector is already written, more tests will be performed and support for
PF/VF will be added.