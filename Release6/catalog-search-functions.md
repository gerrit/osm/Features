# Catalogue Search Functions #

## Proposer ##

Francisco Rodriguez (Indra)

## Type ##

**Feature**

## Target MDG/TF ##

osmclient, NBI

## Description ##

Invoking service primitives on a Network Service or getting Monitoring
Parameters requires to provide the nsrid or vnfrid, which are returned after
instantiation. If the system that invokes the primitives later is not the one
that launched the instantiation, the nsr-id/vnfr-id will not be available.

In order to retrieve those ids is necessary to have a method that returns a
single VNFR or NSR, looking by Network Service Name, instead of the existing
operation that returns the whole catalog.

## Design ##
Design pad: https://osm.etsi.org/pad/p/feature1400

While this feature is not implemented, the python osmclient library can be
leveraged to do a search by name. Behind the scenes, the osmclient library
is getting the whole catalogue and picking the requested record from name.

The SO will add the capability to specify a search with any non-key fields.
This has the benefit of being more future proof. This capability will be used
in combination with a new function at osmclient for doing a lookup with
filtering capabilities.

## Demo or definition of done ##

An external application such as a provisioning system will be able to use the
northbound interface to "create a new corporation" by recovering the relevant
NSR first.