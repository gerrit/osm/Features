# Detection of VIM events not directly related to VDU infra metrics #

## Proposer ##
- Gianpietro Lavado (Whitestack)

## Type ##
**Feature**

## Target MDG/TF ##
MON

## Description ##

Detection of VIM events not directly related to VDU infra metrics (broken VMs,
links/networks down, etc.)

OSM should be able to detect the following VIM events:
1. VM state change (shutdown, pause, not present, etc.)
2. Network changes (deletion, disconnection, etc.)
3. Volume changes (detachments, deletion, etc.)
4. VIM status (API connectivity)

## Demo or definition of done ##
The following events should make MON throw a log related to the NS, and event in bus.
- A VM is powered down at the VIM interface.
- A network is disconnected from a VDU.
- A volume is detached from a VDU.
- The VIM API is not available.
