# Basic functionality for OSM Performance Management in LW-UI #

## Proposer ##
Francesco Lombardo
Stefano Salsano

## Type ##
**Feature**

## Target MDG/TF ##
LW-UI

## Description ##
The goal is to implement in the LW-UI new actions for the NS instances
exploiting the NBI calls for performance and fault management. This includes:
* Alarm creation
* One-time metric export
* Request for starting continuous metric export
* Request for stopping continuous metric export

No impact is expected in other modules.
