# Initial support for config drive in VMware VCD VIM connector #

## Proposer ##

- Vanessa Little (VMware)
- Matt Harper (RIFT)
- Ravi Chamarty (RIFT)

## Type ##
*Feature**

## Target MDG/TF ##
RO

## Description ##
Many VNF vendors support processing of userdata/cloud-init in their VMs. VCD 9.x currently does not support explicit userdata injection. However, such a functionality can be implemented by implementing a config drive in the VCD VIM connector. In the first phase, we plan to support simple userdata and SSH keys. The following workflow is expected
- Generate cloud-init version 2 directory structure as per below reference
    https://cloudinit.readthedocs.io/en/latest/topics/datasources/configdrive.html
- VIMconnector to generate ISO image
- Create image in VCD organization catalog
- VIMconnector to launch VM with additional disk based on above image

## Demo or definition of done ##
Verify parsing of userdata using cirros and Ubuntu-16 VMs
IP address generation and multi-disk based VMs are not supported in this phase.
