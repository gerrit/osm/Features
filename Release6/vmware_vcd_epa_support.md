# Support for EPA support #

## Proposer ##

- Vanessa Little (VMware)
- Matt Harper (RIFT)
- Ravi Chamarty (RIFT)

## Type ##
**Feature**

## Target MDG/TF ##
RO

## Description ##
Add support for VCD EPA support
- NUMA affinity
- CPU pinning

## Demo or definition of done ##
Verify OSM R5 descriptor packages using VCD 9.5
