# Support of multi-segment VIM-managed networks #

## Proposer ##
- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type ##
**Feature**

## Target MDG/TF ##
RO

## Description ##
A VIM, with the help of an SDN controller, can allow the creation of
multi-segment networks that enables seamless connectivity between legacy VLAN
domains (e.g. external networks aka provider networks), SR-IOV or Passthrough
interfaces, and VIRTIO interfaces.

This can be done, for instance, by deploying a VXLAN gateway on the physical
switches, which must support VXLAN encapsulation and HWVTEP functionality. The
gateway would be created by the SDN controller, which is controlled by the VIM.
In the case of Openstack, network configuration is performed using neutron’s
multi-segment networks and the L2GW service plugin.

This feature will modify the way networks are created in a VIM, assuming that
the VIM supports multi-segment networks. There are three cases:
1. Networks where only VIRTIO interfaces are connected
2. Networks where only dataplane interfaces (SR-IOV or passthrough) are
connected
3. Networks where a mix of VIRTIO and dataplane interfaces are connected

In case 1, it is foreseen that the network is created as a single-segment
network. In cases 2 and 3, it is foreseen that the network will be created as
multi-segment network. The reason is that VLAN identifiers are a scarce
resource, while VXLAN Network Identifiers are not. Allocating a VLAN for case 1
could imply running out of VLANs very quickly, since networks for case 1 are
typical for cloud application workloads.

In cases 2 and 3, it is foreseen that it would be possible to connect, a
posteriori, new elements to the already created multi-segment networks.

## Demo or definition of done ##
To be added.
