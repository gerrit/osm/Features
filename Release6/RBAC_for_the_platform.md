# RBAC for the platform #

## Proposer ##
- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type ##
**Feature**

## Target MDG/TF ##
SO

## Description ##
The NFV Orchestrator requires a significant set of capabilities and privileges 
to perform all its required tasks: VNF onboarding, NS design & onboarding, NS 
deployment, day-2 operation, NS shutdown, or addition of new datacenters/VIMs, 
among others. However, not all of those tasks are expected to be performed by 
the same user in the organization, since each of those stages may have 
different implications in terms of service continuity, validation, license 
consumption, access to credentials, etc.

Thus, for real operation, the system should allow the definition of different 
roles, defined by admin user, with different sets of privileges. All users 
should be mapped, at least, to one of these roles.

As a minimum, it is expected that the system should be able to enforce these 
privileges:
1. Allowed to onboard a VNF
2. Allowed to onboard a NS
3. Allowed to deploy a NS
4. Allowed to operate an existing NS (call to primitives, receive monitoring 
data, etc.), except NS scaling.
5. Allowed to scale a NS.
6. Allowed to terminate a NS.
7. Allowed to customize the system and configure the roles.

By default, the admin/root role should have been assigned all the privileges 
above.

## Demo or definition of done ##
- Successful creation by an admin user of the role TECHNOLOGY with privileges 
#1, #2, #3, with an user (tech) on it.
- Successful creation by an admin user of the role OPERATIONS with privileges 
#3, #4, #5, #6, with an user (op) on it.
- Check that tech and op are allowed to run operations of the kind authorized 
in their role.
- Check that tech and op are not allowed to run operations not authorized in 
their role.
- Check that users with the admin role support all the types of operations 
above (from #1 to #7).