# Juju Asyncronous API #

## Proposer ##
- Adam Israel

## Type ##
**Feature**

## Target MDG/TF ##
SO

## Description ##

As of R2, the SO is using the older syncronous Juju API. This can lead the SO
to appear "frozen" while it blocks waiting for a Juju operation to complete.

To address this issue, we would like the SO to convert to using
[libjuju](https://github.com/juju/python-libjuju) python library. It offers
the following benefits:
- Asynchronous, using the asyncio and async/await features of python 3.5+
- Websocket-level bindings are programmatically generated (indirectly) from the
Juju golang code, ensuring full api coverage
- Provides an OO layer which encapsulates much of the websocket api and
provides familiar nouns and verbs (e.g. Model.deploy(), Application.add_unit(),
etc.)

## Demo or definition of done ##
Demos of using libjuju can be found in the
[quickstart](https://github.com/juju/python-libjuju#quickstart) and in the
[examples](https://github.com/juju/python-libjuju/tree/master/examples) folder.

Here is a simple implementation that deploys a charm:
```python
#!/usr/bin/python3.5

import asyncio
import logging

from juju import loop
from juju.model import Model


async def deploy():
    # Create a Model instance. We need to connect our Model to a Juju api
    # server before we can use it.
    model = Model()

    # Connect to the currently active Juju model
    await model.connect_current()

    # Deploy a single unit of the ubuntu charm, using revision 0 from the
    # stable channel of the Charm Store.
    ubuntu_app = await model.deploy(
        'ubuntu-0',
        application_name='ubuntu',
        series='xenial',
        channel='stable',
    )

    # Disconnect from the api server and cleanup.
    model.disconnect()


def main():
    # Set logging level to debug so we can see verbose output from the
    # juju library.
    logging.basicConfig(level=logging.DEBUG)

    # Quiet logging from the websocket library. If you want to see
    # everything sent over the wire, set this to DEBUG.
    ws_logger = logging.getLogger('websockets.protocol')
    ws_logger.setLevel(logging.INFO)

    # Run the deploy coroutine in an asyncio event loop, using a helper
    # that abstracts loop creation and teardown.
    loop.run(deploy())


if __name__ == '__main__':
    main()
```
