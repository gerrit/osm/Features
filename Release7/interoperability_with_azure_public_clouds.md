# Interoperability with Azure public clouds #

## Proposer ##
- Alfonso Tierno (Telefonica)
- Gerardo Garcia (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type ##

**Feature**

## Target MDG/TF ##

RO

## Description ##

Currently OSM can deploy VNFs in VIMs of the following types: VMware, 
Openstack, Openvim and AWS.
With this feature the public cloud portfolio will be extended to Azure. 
This will ease the adoption and testing of OSM. Azure public clouds 
could be part of multi-site deployments in production.

In practice, this feature would require interacting with Azure, in a 
similar way as it is done with AWS.

## Demo or definition of done ##

Deploy a NSD example with at lease one VM and one private network at public 
cloud Azure
