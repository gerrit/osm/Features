# OSM Kubernetes Support #

## Proposer ##
- Prithiv Mohan (Intel)

## Type ##
** Feature **

## Target MDG/TF ##
SO, RO, VCA, IM

## Supported VIMs ##

1. OpenStack
2. VMWare
3. AWS

## Description ##

Enable a container-based VNF deployment model via OSM that allows for the coexistence of container
and VM deployment models to coexist within a network service. Kubernetes should be enabled as the
container orchestration engine, but it must be done in a plugin manner that permits other container
orchestration engines to be added in the future.

The Kubernetes pods can be created either on a baremetal node or inside a VM. Either case, the
Resource Orchestrator(RO) is responsible only for the resource allocation.

For the VM based Kubernetes deployment, the VM image could have the kubelet node agent already
installed. In this case, RO creates a VM and VCA takes care of the configuration and the LCM of the
pods. If the image used by the RO to provide a VM has no kubelet installation, the VCA module will
handle the installation, configuration of the kubernetes. The Resource Orchestrator will not involve
in the installation of any kind of Kubernetes package.

The options for baremetal provisioning includes OpenStack Ironic, Canonical Metal as a Service(MaaS)
but not limited to that. An additional or extended plugin is required in the Resource Orchestrator
for the baremetal provisioning of the resources for the Kubernetes.

The proposed workflow below assumes Ironic/MaaS as the options for baremetal provisioning:

	i.    User creating a descriptor with VNFD updates that indicate the deployment model for the
	      VNF (container on bare metal, container in VM, or VM deployment.
	ii.   SO passes the request to RO.
	iii.  RO receives the request.
        iv a. In case of a Kubernetes deployment inside the VM, RO creates a VM with/without kubelet
              installed in it based on the type of the image.
           b. Alternatively, in the case of a Kubernetes baremetal deployment, RO communicates with
              the MaaS/Ironic plugin. The plugin talks to the MaaS and/or Ironic to create the
              Kubernetes pods.
        v.    VCA does the installation and/or configuration of the VM for Kubernetes.

Changes in the descriptor in the IM repo is required. Expected these show up fairly seamlessly in
the UI due to the model driven nature of the platform.

There is an option on the preparation of the baremetal nodes or VMs for supporting Kubernetes. These
could be prepared beforehand, or Kubernetes could be configured on them once they have been
allocated.

The installation of Kubernetes and the configuration of the Kubernetes Controller will not be handled
by the OSM as it is out of scope for OSM. The configuration could be handled by Juju with the charms.

## Demo or definition of done ##

* Creation of Kubernetes Pods is successful
* Usual lifecycle operations on a NS that includes VM-based and container-in-VM based VNFs.
* Supports multiple VIM environments.

Links:

1. https://kubernetes.io/
2. https://kubernetes.io/docs/getting-started-guides/ubuntu/installation/
3. https://www.ubuntu.com/kubernetes
