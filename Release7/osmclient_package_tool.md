# osmclient package creation and validation tool

## Proposers

- Felipe Vicens (ATOS)
- Gerardo Garcia (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type

Feature

## Target MDG/TF

Devops/osmclient

## Description

The creation of the package in OSM currently is made via a DevOps package tool located in DevOps repository.
This feature aims the migration of the bash script to the osmclient in order to have it integrated with 
the osm command-line tool. Additionally, this feature proposes the syntax validation of the descriptors as well as 
the generation of the tar.gz

## Demo or definition of done

The execution of the following commands using the OSM client to create, validate or build descriptors

- 'osm package-create': Creates an OSM package for nsd, vnfd and nst
- 'osm package-validate': Syntax validation of OSM descriptors
- 'osm package-build': Generate .tar.gz files of nsd and vnfd
