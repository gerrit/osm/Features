# Store MON alarms in MongoDB

## Proposer
* Benjamín Díaz

## Type
**Feature**

## Target MDG/TF
MON, POL

## Description

This feature aims to modify the current database where alarms are stored in MON from MySQL to MongoDB.

This will simplify MON operation, as it will no longer require a MySQL engine and also will not have to worry about schema migrations.

This will align with other components in terms of enabling creation of resources (alarms, in this case) directly in MongoDB, and having MON begin using them without doing any kind of RPC. POL, for example, will no longer need to communicate via Kafka to create alarms in MON, but will be able to just create the in the DB, as it is considered a shared resource between all modules.

## Demo or definition of done

Alarms are being created in MongoDB.

All alarm functionality is not affected by this change.

