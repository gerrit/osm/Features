# Improved scalability of OSM platform #

## Proposer ##
- Francisco Javier Ramon (Telefonica)
- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)

## Type ##
**Feature**

## Target MDG/TF ##

All components deployed in OSM platform (LCM, RO, NBI, MON, POL, UI, Mongo DB, MySQL DB, Keystone)

## Description ##
**This feature obsoletes feature #666: https://osm.etsi.org/gerrit/#/c/666**

OSM should be capable of increasing its resources via scaling-out components
whenever possible.

In order to make these strategies possible, it might be desirable:
- Identifying which components (inside each of the current modules) are
intended to store permanent information (databases, repositories) or should be
considered as stateful, and devise specific HA strategies for them.
- Identifying which components are stateless (or can recover efficiently their
state from databases or stateful components) and devise bootstrap and load
balancing procedures for them.
- Identifying the flow by which databases and the state of the different
components should be populated after a scale-out.

## Demo or definition of done ##

- The feature will be considered done when the instructions on how to scale-out
and load balance each module are captured in the OSM wiki or any other OSM
document. On-demand load balance, if available, should be also properly
documented.