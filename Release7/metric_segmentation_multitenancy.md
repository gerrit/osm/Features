# OSM Metric segmentation & access by project

## Proposers

- Gianpietro Lavado (Whitestack)
- Gerardo Garcia (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type

Feature

## Target MDG/TF

SA

## Description

Metrics in Prometheus are stored in key-value pairs, grouped per name and differentiated by labels.
Current design keeps metrics grouped by name (i.e. cpu_usage) and allows to differentiate based on
label filtering (i.e. ns_id), so for example, a Grafana dashboard can be created for a single NS ID,
filtering by the metric's label "NS_ID".

Still, current design has some limitations:
- VIM metrics have different names by default and refer to the same resource for any NS/VNF, but
VNF Metrics (Indicators) using the same name, could have different semantics depending on the VNF,
or worse, different types (gauge and counter for example), and may not be supported.
- There is no role-based authentication or tenant awareness. Prometheus has some limitations, but
Grafana may solve some of ths issues at a higher level by automating the creation of dashboards.

This feature presents an opportunity to revisit metric segmentation, authentication, and dashboard
automation.

## Demo or definition of done

OSM users should be able to see the metrics beloging to their own NS, automatically, in Grafana.