# Osmclient migration to Python3

## Proposers

- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type

Feature

## Target MDG/TF

Devops

## Description

Python 2 End of Life is expected for January 1st 2020. We need to address
the migration to Python3 before that date.

## Demo or definition of done

- A new debian package will be produced: python3-osmclient
- The new debian package will be used by the osmclient Dockerfile in Devops stage3.

