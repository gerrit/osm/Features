# Infrastructure monitoring with bare Ceilometer in OpenStack environments

## Proposer

- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type

**Feature**

## Target MDG/TF

MON

## Description

Current monitoring capabilities for VDU resources deployed in OpenStacks, 
requires the availability of AODH/Gnocchi in the VIM. However, not all 
OpenStack VIMs (particularly, legacy ones) include these modules, but provide 
just bare Ceilometer.

This feature requests the capability to support resource monitoring based 
exclusively in the service provided by Ceilometer, even if it might pose some 
limitations.

## Demo or definition of done

Check that the current options to obtain VDU metrics/alarms in OpenStack VIMs 
are also available in an OpenStack with Ceilometer but without AODH/Gnocchi.
