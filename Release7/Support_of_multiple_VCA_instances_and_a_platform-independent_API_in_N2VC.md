# Support of multiple VCA instances and a platform-independent API in N2VC

## Proposers

- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)
- Adam Israel (Canonical)
- José Antonio Quiles (Indra)

## Type

Feature

## Target MDG/TF

N2VC, LCM

## Description

Currently, OSM assumes that management networks of all the VNF instances are remotely accessible
by OSM from outside the datacenter, either directly or via floating IPs, so that VCA can drive
successfully its operations with the VNFs. While this way of work has many advantages in telco
clouds it might be somewhat rigid in hybrid deployments where e.g. public clouds are involved. In
fact, this imposes some networking conditions to OSM so that VCA can access to all the
management networks of each target VIM.

In addition, the current N2VC library is platform-dependent. Most of the API calls are juju-specific
instead of providing a higher level interface.

While this shouldn't be an issue from the point of view of the actual behaviour of the OSM platform
as a whole, it has three main drawbacks:

- Architecturally speaking, it breaks the general principles of abstraction and layering, percolating
  terminology and way of thinking northbound.
- It complicates the implementation of features in LCM, since LCM needs to understand all the internals
  of each call and figure out the actual state of N2VC and VCA internals.
- It locks the workflow in LCM, which cannot be modified without changing N2VC. As a result, almost
  every new feature impacting N2VC (support of native charms, secure key management, support of
  relations, NS primitives) forces to change in the workflow in LCM, as current workflows are entirely
  VCA-oriented (rather than LCM-oriented) due to the aforementioned problem in layering.

This feature is intended to re-define the N2VC API to restore the layering, make it truly
platform-independent (agnostic of the VCA terminology), and able to support multiple VCA instances.
It will require the implementation of a generic class in N2VC, defining those API calls and the
expected behaviour.

The main premises of the new N2VC API would be the following:

- **High-level abstraction API.** The current platform-dependent objects managed by N2VC (machines,
  charms, models, applications) will be replaced by a new set of generic platform-independent objects
  (execution environment, configuration SW, namespaces).
- Keep a decoupling among the elements to be configured, the configuration SW and the execution
  environment, so that the workflows in current VCA can be reproduced.
- **API calls should be defined in such a way that allows to do things in parallel.** For instance, it
  is not necessary to wait for the RO to finish the instantiation to before makeing VCA to create the
  execution environments, or vice-versa.
- **Layering and isolation.** LCM workflow (sequence of N2VC calls) could change in the future and it
  should not impact N2VC or vice-versa.
- **Transparency in progress of operation and states.** N2VC should be able to update directly the Mongo
  records associated to the operation in progress.

## Demo or definition of done

All DEVOPS tests using VNF/NS packages with charms should work in the same way as they are working today.

