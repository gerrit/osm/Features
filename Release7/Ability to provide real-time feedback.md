# Ability to provide real-time feedback in CLI and GUI upon request

## Proposer

- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type

**Feature**

## Target MDG/TF

IM-NBI, LCM, CLI, LW-GUI (optional)

## Description

Currently, OSM's CLI (and OSM's GUI to some extent) follow a similar behaviour 
to OSM's NBI, returning control to the user immediately after a request has 
been made, although OSM is still being processed by OSM behind the scenes, so 
the user needs to poll the system to learn (i.e. running commands) when the 
request has been completely processed. While this behaviour is reasonable as 
regular practice, it still presents some limitations in terms of usability:

- It makes more complicated to follow the progress of a given operation or 
whether it is stuck at any step, particularly when the requested operation is 
complex (e.g. instantiate a large NS).
- It makes even more complicated to relate the processes during the operation 
with other asynchronous events (e.g. error messages) that may happen during the 
operation and explain better its progress and/or results.

The current proposal is adding an option `--wait` to selected CLI commands so 
that they do not return the control immediately, but keep it until the 
operation is completed, reporting meanwhile the progress in the operation and, 
if appropriate, relevant events that may facilitate diagnosis/troubleshooting.

As a minimum, the commands for NS instantiation and NST instantiation should 
support this option. Likewise, LW-GUI might support the same mode for 
equivalent operations.

## Demo or definition of done

Possibility to use the `--wait` option for an NS/NSI creation that keeps 
control and reports the progress of the operation in terms of:

- VMs/VDUs created, VLs created and bar of progress (e.g. "VM 7/24: 'CTRL_1' 
VDU of 'Router_7' VNF created").
- Stage of instantiation where the process is (e.g. "starting NS day-1", 
"Starting 'Router_2' day-1", etc. ).
- Timely report any other relevant event that might occur in the middle of the 
operation.

