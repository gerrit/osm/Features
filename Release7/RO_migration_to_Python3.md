# RO migration to Python3

## Proposers

- Alfonso Tierno (Telefonica)
- Gerardo Garcia (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type

Feature

## Target MDG/TF

RO, Devops

## Description

Python 2 End of Life is expected for January 1st 2020. We need to address
the migration to Python3 before that date.

## Demo or definition of done

- RO will run on python3. RO client will also run on python3.
- A new debian package will be produced "python3-osm-ro", totally based on Python3,
  with no dependences on Python2 or Python2 libraries.
- The new debian package will be used by the RO Dockerfile in Devops stage3.

