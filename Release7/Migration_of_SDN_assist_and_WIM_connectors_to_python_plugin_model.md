# Migration of SDN assist and WIM connectors to python plugin model

## Proposers

- Alfonso Tierno (Telefonica)
- Gerardo Garcia (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type

Feature

## Target MDG/TF

RO, Devops

## Description

RO is currently keeping two sources for plugins:

- SDN assist plugins hosted in openvim repo
- WIM plugins in RO repo

With the new feature in progress on high level primitives for SDN assist,
it is identified the need and advantage of having all the plugins in the
RO repo, removing the dependency on openvim in the RO.

This feature will enable that each existing and future connector for low-level
SDN assist, high-level SDN assist and WIM will be implemented in a dedicated folder
in the RO, with its own setup.py so that a specific debian package will be
generated for each plugin.

## Demo or definition of done

- A new debian package will be produced for each existing SDN assist and WIMconnector:
  python3-XXXX-plugin
- All the new debian packages will be used by the RO Dockerfile in Devops stage3.

