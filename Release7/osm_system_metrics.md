# osmclient package creation and validation tool

## Proposers

- Gianpietro Lavado (Whitestack)
- Gerardo Garcia (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type

Feature

## Target MDG/TF

SA

## Description

Current TSDB collects to VIM, VM and VNF metrics through a customized prometheus exporter in MON 
(that needs to evolve on its own, focusing on Network Services)

For system metrics, including host(s) and container stats, standard prometheus exporters could be
included and integrated to the same Prometheus TSDB.

Even though the same standard Prometheus exporters would apply to any OSM installation type,
the way of installing the exporters would vary according on the OSM installation method:
(a) Single-node OSM based on Docker Swarm
(b) Multi-node OSM based on Kubernetes
(c) OSM Distribution

Grafana dashboards would be included by default, the only variable that could change across 
installations is the configuration of the datasource.

A natural consequence of this feature would be to promote Grafana to the default OSM stack.

## Demo or definition of done

After installing OSM, Grafana is available by default, and system metrics appear in a dashboard.