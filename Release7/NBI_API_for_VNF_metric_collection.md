# Enhancement of NBI API to collect VNF metrics

## Proposers
- Vijay R S (Tata Elxsi)

## Type

**Feature**

## Target MDG/TF

NBI

## Description

Present API implementation for NS metric collection in NBI(Feature 7270) only fetch NFVI metrics, 
not the VNF metrics.

NFVI metrics listed as,
 cpu_utilization
 average_memory_utilization
 disk_read_ops
 disk_write_ops
 disk_read_bytes
 disk_write_bytes
 packets_dropped_<nic number>
 packets_received
 packets_sent

Improvising the NBI to fetch both VNF and NFVI metrics from Prometheus as part of the same API.

This can be done by making NBI fetch VNFRs collection from mongodb and get the VNF metric names 
with that NBI can query Prometheus to get the VNF metrics.

## Demo or definition of done

Working NBI API which will fetch metrics of both NFVI and VNFs. 