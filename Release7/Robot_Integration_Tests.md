# Robot integration framework for builds verification #

## Proposer ##
Jayant Madavi
Mrityunjay Yadav

## Type ##
**Feature**

## Target MDG/TF ##
**DevOps**

## Description ##
Currently OSM supports many types of [VIMs](https://osm.etsi.org/wikipub/index.php/VIMs) ex Openstack, VMware, Openvim, AWS etc. For each supported VIMs test cases are executed manually. Today for each release, based on the features/bug fix, test cases are identified and results are captured in [xls](https://osm.etsi.org/wiki/index.php/Release_SIX_Integration_(DEVOPS)). These test cases can be classified based on VIMs as Core_TestCases, featureOrBug_TestCases. 
Using robot framework  will ease certifying the build. Also at the same time will create incremental repository of comprehensive test cases, for the upcoming build/releases. 
Robot framework is going to generate the report, after every stage-4 build. Based on these reports, a build can be classified as Successful, partially successful or failed.   

## Demo or definition of done ##
Run the agreed set of test cases for each build/release. Check the output report for success/failure. 