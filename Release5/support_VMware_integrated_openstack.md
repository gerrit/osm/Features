# Support VMware Integrated Openstack as a VIM  #

## Proposer ##
Vanessa Little (TDC,VMware)

## Type ##
**Feature**

## Target MDG/TF ##
RO

## Description ##
Regression test the current generic openstack library against VMware Integrated Openstack version 2.0 (kilo) (which is included in the current release of vCloud NFV 1.5)

If tests are successful a separate openstack connector should not be required to connect to VMware Integrated Openstack as a VIM.  However, the results of the regression tests will determine the detailed requirements and feature roadmpa for Relase 4

## Demo or definition of done ##
Definition of Done:  Network services can be successfully deployed in VMware Integrated Openstack with the same connector, Network Service and VNFD descriptors as generic openstack builds that are based on Openstack Kilo.