# NS instance topology view in LW-UI #

## Proposer ##
Francesco Lombardo
Gerardo Garcia

## Type ##
**Feature**

## Target MDG/TF ##
LW-UI

## Description ##

The LW-UI does not have today an NS instance topology view. This
feature request is about implementing this view integrated in the LW-UI.

No impact is expected in other modules
