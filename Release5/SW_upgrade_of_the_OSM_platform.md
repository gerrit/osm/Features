# SW upgrade of the OSM platform #

## Proposer ##
- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type ##
**Feature**

## Target MDG/TF ##
SO, RO, VCA

## Description ##
The NFV Orchestrator, as any other complex system in production, is expected to
require regular SW upgrades, including bug fixes, security patches or new
functionality.

A mechanism should exist to guarantee that SW upgrades in the platform do not
disrupt service continuity, providing also a safe rollback mechanism in case of
failure. In both cases, the system should be able to restore its last known
internal state.

## Design ##
Design pad: https://osm.etsi.org/pad/p/feature1419

## Demo or definition of done ##
In a running OSM system with an instantiated NS, a SW upgrade is triggered.
Once complete, two checks are expected:
- The SW upgrade is successful and the system is able to restore the last known
state, allowing the operation of the pre-existing NS instance again.
- A rollback operation is triggered and the system returns to the previous SW
version, keeping the state and still allowing the operation of the NS.

The procedure should be captured in this page or a similar one in the public
wiki for the active release:
https://osm.etsi.org/wikipub/index.php/Software_upgrade_(Release_THREE)
