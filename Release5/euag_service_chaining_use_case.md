# Service chaining use case #

## Proposer ##
**EUAG**

## Type ##
**Feature**

## Target MDG/TF ##
LCM, RO

## Description ##
A SP wants to offer a service to corporations consisting of a VPN service with 
access to different types of endpoints (other corporate sites, Internet or 
public cloud) where each corporation can insert a custom chain of VNFs in the 
middle.

Selected traffic from corporates can reach an NFV datacenter. Once in the 
datacenter, the corporation can select which chains to be used for each traffic
flow (i.e. classified) depending on e.g. the source and destination of the 
traffic. For instance:

* FROM Corporate site TO Amazon: chain A
* FROM Corporate site TO Internet: chain B
* FROM Corporate site TO Corporate site: chain C 
* FROM a VPN connection from an employee TO Internet: chain D
* FROM a VPN connection from an employee TO Corporate Site: chain C

## Demo or definition of done ##
N/A

