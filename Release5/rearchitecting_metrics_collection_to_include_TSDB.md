# Re-architecting metrics collection to include TSDB #

## Proposer ##
- Gianpietro (Whitestack)

## Type ##
**Feature**

## Target MDG/TF ##
MON, DEVOPS, IM

## Description ##

OSM MON needs a way to store metrics coming from different sources, such as VIM or, in the future, VNFs.  It also could benefit from the correlation that may happend between both sources of information.
This was described during OSM F2F MR#5 Oslo and detailed here: https://docbox.etsi.org/OSG/OSM/05-CONTRIBUTIONS/2018/OSM(18)000074_OSM_PM___FM_Architecture_Proposal_4_.pdf

As agreed, OSM will have a common TSDB implemented using Prometheus (Apache2 license), which will be used to:
1. Store metrics gathered by MON from VIM and (directly from) VNFs (through N2VC)
2. Correlate metrics coming from the different sources in the context of every NS/VNF.
3. Implement its own alarming service in order to monitor the status of the metrics and trigger its own alarms to avoid dependancy of limited VIM alarming services like OpenStack Aodh.
4. Expose an well-known API (prometheus) which can be used by well-known tools (like Grafana) to build a metrics dashboard.
5. Be queried by MON to respond to 'instantaneous metric values' requests to the kafka bus, when requested from OSM CLI or NBI.

## Demo or definition of done ##
During OSM installation, users should be able to:
- Include an optional 'grafana' stack which will install Grafana (Prometheus will be included by default as a common service)

During NS onboarding, users should be able to:
- Activate VNF/VDU monitoring (deactivated by default), at descriptor level.

During NS instantiation, users should be able to:
- Activate VNF/VDU monitoring to override what was available at descriptor level.

After NS instantiation, and if monitoring was activated, users should be able to:
- Query Prometheus through OSM CLI or NBI for instantaneous metrics.
- Query Prometheus directly for historical metrics.
- (If Grafana installed) Open Grafana and see a pre-populated dashboard as example.