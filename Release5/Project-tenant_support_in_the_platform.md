# Project/tenant support in the platform #

## Proposer ##
- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type ##
**Feature**

## Target MDG/TF ##
SO

## Description ##
The NFV Orchestrator should be allowed to access all the datacenters and 
services in a carrier network. However, from the perspective of the real 
operation, it is often desirable to allow the partitioning of the telco cloud 
space in different projects/tenants, following an equivalent approach to the 
one at VIM layer.

Thus, it would be desirable that an OSM project/tenant provided a separate 
space with:
- A given set of accessible VNFs
- A given set of accessible NSs
- A set of running NS
- Optionally, a number of authorized datacenters (if more than one).

VNF descriptors and NS descriptors might be declared public (available for all 
tenants) or available for a list of tenants. An NS instance will only run in 
one tenant.

Users should belong at least to one tenant. By default, admin users should be 
authorized in all tenants.

## Demo or definition of done ##
- Create a 2 tenants (A and B) and add some users to them (user_a1, user_a2, 
and user_b1). Assign different datacenters to them.
- Check that user_a1 can onboard a VNF and a NS in the tenant A successfully. 
Check that it is not visible or accessible by user_b1.
- Check same operation as above but by user_b1 in tenant B.
- Check that user_a1 can deploy successfully its NS in the tenant A. Check that 
the NS is not visible or accessible by user_b1.
- Check that user_a2 can access to the same VNFDs, NSDs, and the running NS 
available it tenant A.
- Onboard an additional VNFD and a new NSD that uses the VNF and make them 
public (e.g. from an admin user).
- Check that the public NSD and VNFD are accessible from both tenants and that 
the NS can be successfully instantiated from any of them.
- Check that each tenant is deploying their NS instances in their corresponding 
datacenter.