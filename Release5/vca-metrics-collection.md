# Metrics Collection #

## Proposer ##
- Adam Israel

## Type ##
**Feature**

## Target MDG/TF ##
VCA, DevOps, SO

## Description ##
Operators prefer to make data-driven decisions. To aid this, I propose that we
begin collecting operational metrics from VNFs that make them available, via
push (collectd, statsd) or pull ([prometheus](https://prometheus.io/), etc).

This metric data should be collected to a central data store. Once collected,
reports can be generated for the operator to view, and operational rules can be
defined with regard to notifications, auto-scaling and auto-healing.


## Demo or definition of done ##
- Selection of supported collection method(s)
- Creation of an OSM VNF METRICS charm layer to aid metrics collection
- The option to install metric collection during the installation process.
- Exposing a metrics dashboard (if supported by the supported collection
    method(s), or the definition of of one or more reports to view relevant
    metrics dtaa.
