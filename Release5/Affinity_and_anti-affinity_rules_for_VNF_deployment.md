# Affinity and anti-affinity rules for VNF deployment #

## Proposer ##
- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type ##
**Feature**

## Target MDG/TF ##
SO, RO

## Description ##
OSM should provide means to specify that different VDUs of the same VNF should 
be deployed in different availability zones of the same datacenter. This 
behaviour is often required by VNFs that implement active-standby resiliency 
strategies.

Current OSM�s IM might need to be extended to support this feature.

## Demo or definition of done ##
- Add a datacenter to OSM specifying more than one availability zone.
- Add a second datacenter with only one zone.
- Onboard a VNF with 2 VDUs, with a VNFD that mandates that these VDUs must be 
deployed in different availability zones.
- Onboard an NS which includes the VNF above.
- Instantiate the NS in the datacenter with 2 availability zones and check that 
the VDUs are deployed as expected.
- Instantiate the NS in the datacenter with one zone and check that it fails.