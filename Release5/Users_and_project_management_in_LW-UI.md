# Users and project management in LW-UI #

## Proposer ##
Francesco Lombardo
Stefano Salsano

## Type ##
**Feature**

## Target MDG/TF ##
LW-UI

## Description ##
The goal is to add users and project management in LW-UI aking use of OSM NBI
calls for that purpose.

No impact is expected in other modules
