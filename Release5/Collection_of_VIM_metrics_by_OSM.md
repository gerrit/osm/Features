# Collection of VIM metrics by OSM #

## Proposer ##
- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type ##
**Feature**

## Target MDG/TF ##

MON

## Description ##

OSM should provide means to receive selected monitoring parameters from the VIM 
(incl. infrastructure) that can be taken into account for the orchestration 
process or even fed to external systems.

OSM should aim to consume information provided by existing and well-known 
frameworks of monitoring (e.g. Ceilometer, Nagios) as well as others to come, 
leveraging on a specific plugin model that enables a uniform modeling 
northbound.

OSM should be able of relating this monitoring information to existing VNF 
instances and their corresponding NS instances.

Current OSM's IM will have to be extended to support this feature.

## Demo or definition of done ##

- VNFD can include a list of selected infrastructure monitoring parameters that 
are relevant for their VDUs.
- Check that this VNFD, in the corresponding VNF Package, can be successfully 
onboarded.
- Check that it is possible to assign a monitoring framework per OSM datacenter.
- Deploy an NS which includes the VNF above.
- Check from the northbound interface that it is possible to discover the list 
of infra-related parameters that can be monitored for a specific VNF in the NS 
instance.
- Check that, using the northbound interface API, it is possible to request 
that some of those monitoring parameters can be fed to an external system that 
e.g. can display the variation.
- A single VIM plugin would be considered enough (e.g. for Openstack)