# VNF and NS composer in LW-UI #

## Proposer ##
Francesco Lombardo
Stefano Salsano

## Type ##
**Feature**

## Target MDG/TF ##
LW-UI

## Description ##
The LW-UI does not have today a VNF and NS composer tool in the LW-UI. This
feature request is about implementing this tool integrated in the LW-UI.

No impact is expected in other modules
