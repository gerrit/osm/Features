# Auto-scaling VNF with horizontal scale out of VDUs #

## Proposer ##
**EUAG**

## Type ##
**Feature**

## Target MDG/TF ##
NBI, UI, LCM, RO, VCA

## Description ##
Some VNFs have the capability to scale by adding more VDU/VM without adding new 
internal VLDs and without changing the external connection points of the VNF, so 
that this scale out has no effect in other VNFs of a NS. This is the commonest 
case of scale out in cloud, so it is the most basic case to enable dynamicity in 
the use of resources. This feature aims to enable this capability in OSM.

The end user might use the UI to request, for a given NS, an action to scale a 
VNF that is part of that NS. This action would be shown to the end user only if 
the VNF has the capability to scale.

In other cases, the own VNF might have been authorized to trigger (or suggest) a 
scale out event based on pre-defined KPIs thresholds.

Both use cases for driving scaling are considered of relevance for the project.

## Demo or definition of done ##
N/A
