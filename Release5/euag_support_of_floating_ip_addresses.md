# Floating IP addresses in VNF interfaces for public access from outside a DC #

## Proposer ##
**EUAG**

## Type ##
**Feature**

## Target MDG/TF ##
RO

## Description ##
Currently OSM allows creating and using VIM networks, but it does not allow
assigning a floating IP address to a VNF interface in order to make that VNF
publicly available from outside the datacenter.

The end user should be able to use the UI to assign, during instantiation time
or when updating a running NS instance, a floating IP address to a VNF
interface. The VNF interface will map implicitly to a VM interface, which will
be the interface where the floating IP address will be actually enforced.
The floating IP address should come from the pool of floating IP addresses in
the datacenter where the VNF is deployed.

## Demo or definition of done ##
N/A

