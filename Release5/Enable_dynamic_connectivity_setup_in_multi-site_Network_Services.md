# Enable dynamic connectivity setup in multi-site Network Services #

## Proposer ##
- Oscar Gonzalez de Dios (Telefonica)
- Dimitrios Gkounis (University of Bristol)
- Ramon Casellas (CTTC)
- Rafael Alejandro Lopez da Silva (Telefonica)

## Type ##
**Feature**

## Target MDG/TF ##
IM-NBI, SO/LCM, RO

## Description ##
Current support of multi-site deployments in OSM assume that inter-DC
connectivity is pre-provisioned. This feature tries to cover that gap to
automate inter-DC network creation through a WIM that will cover not only the
WAN deployments, but also will be extended to transport networks in general.

In a multi-site environment, deployments are likely to be geographically
distributed across different sites, interconnected through a (wide area)
transport network. Until recently, provisioning connectivity across such a
transport network has been done in a way that is decoupled from MANO
orchestration and is taken as a static input from its point of view.

For such scenarios, the dynamic provisioning of such a connectivity via OSM
would be highly desirable, and it is in-line with  macroscopic ETSI NFV
architecture that already includes the concept of WIM (WAN Infrastructure
Manager), although the  applicability may not be limited to WAN deployments and
be extended to transport networks in general.

Such transport networks are expected to be SDN controlled. There are (often
competing and overlapping) several alternatives in what concerns the  SDN NBI,
including ONF TAPI (Transport API), IETF NBI models or the already existing
APIs offered by major projects such as ONOS or ODL.

The request is to extend OSM (either developing an additional OSM module or
extending existing ones) to integrate the notion of WIM when a given network
service is deployed in the aforementioned scenario (that is, with NSs that are
deployed across multiple sites). It is thus expected that during the
instantiation, OSM requests connectivity to one (or more) existing WIMs,
implementing a SBI towards them using a plugin model.

The feature thus encompases:
- Identifying in which parts of OSM the concept of delegating connectivity to
an external WIM is needed
- Requesting connectivity to manage inter-DC links between VNFs

 ## Demo or definition of done ##
- Create a with a couple of VNFs (a single VDU would do).
- Create a NSD that includes the two VNFs above and a link between them.
- Instantiate the NSD in such a way that each VNF is placed in a different
DC/VIM, so that the WIM has to create the connectivity between the 2 sites
according to the specified link type and characteristics.
- Check that both of them work properly and can be pinged.
- It would be desirable to test this setup with different types of connectivity
and VM interfaces supported by OSM:
  - Connectivity: E-Line, E-LAN
  - VM interfaces: VirtIO, passthrough, SR-IOV
