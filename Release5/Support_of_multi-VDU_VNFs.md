# Support of multi-VDU VNFs #

## Proposer ##
- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type ##
**Feature**

## Target MDG/TF ##
SO, VCA

## Description ##
Many common use cases involve VNFs that are composed of several types of VDUs 
that need to be scaled and operated independently but still belong to the same 
VNF and, hence, need to be completely described in the VNF Package, not at NS 
level.

Some common examples are composite VNFs -such as IMS or EPC- or simple but 
decomposed VNFs, such as a vRouter with control and data plane that are 
intended to run in separate VMs.

## Demo or definition of done ##
- A VNF Package of a VNF composed of more than one type of VDUs (e.g. 2 types 
of VNUs: user and data plane) is successfully onboarded in the system. The set 
of primitives available is unified at VNF level.
- The VNF is properly deployed, with the VMs interconnected as specified in the 
descriptor and the VNF as a whole can be operated with VNF primitives.

**This feature might obsolete feature #655 _partially_: 
https://osm.etsi.org/gerrit/#/c/655/**