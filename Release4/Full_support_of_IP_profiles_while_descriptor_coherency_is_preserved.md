# Full support of IP profiles while descriptor coherency is preserved #
 
## Proposer ##
- Pablo Montes (Telefonica)
- Antonio Lopez (Telefonica)
- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)
 
## Type ##
**Feature**
 
## Target MDG/TF ##
IM-NBI, SO, RO
 
## Description ##
Many VNFs assume that the VIM can provide specific networking services, for 
their interfaces, their VLs, or both.
 
This feature request aims to review the capabilities that OSM can model and 
control at the levels of VNFD, NSD and instantiation parameters, so that they 
are considered complete and can work coherently.
 
Thus, it is expected that, at VL (VIM network) level, it should be possible to:
- Choose if a DHCP service should be enabled
- Select the CIDR of the network
 
Likewise, it is expected that, at CP (interface) level, it should be possible 
to:
- Select a specific IP address
- Disable port security
 
Besides checking that these capabilities are properly enabled, a coherency 
check should be made in order to prevent that these values can create conflicts 
between different instances of the same NS/VNF or between VNFs interconnected 
in a NS.
 
In order to guarantee that all those properties corresponding to an IP profile 
can only be set in a descriptor where they correspond to VLs or CPs that are 
considered ″internal″ to the corresponding VNFD or NSD. Thus:
* At VNFD level:
  * Only for internal CP, a specific IP address can be set.
  * Port security can be enabled or disabled in ANY port, regardless its 
situation of ″internal″ or ″external″.
  * Only for internal VL (in principle, all of them, in the context of a VNF), 
a specific CIDR can be set or DHCP can be explicitly enabled or disabled.
* At NSD level, same rules as above would apply, but in the context of a NS 
template (now, many ″external″ VNFD’s CPs or VLDs might become 
″internal″ for the NSD).
* If required, parameters passed at the time of instantiation would allow to 
set any remaining parameters, i.e.:
  * Setting IP addresses of external CPs of the NS.
  * Setting the CIDR of external VLs.
  * Enabling/disabling DHCP in an external VL.
 
It must be noted that in those CPs and VLs that are not completely handled by 
the VIM, but by the VNFs + SDN controllers (e.g. with passthrough or SR-IOV are 
involved), some of these parameters might not be applicable.
 
## Demo or definition of done ##
1. Create a VNFD (VNFD_1) with 2 VDUs, each of them with 2 interfaces (besides 
mgmt). One interface of each VDUs is connected to an internal VL between both 
VDUs. This VL will have DHCP disabled and a specific CIDR. The VDU interfaces 
connected to the internal VL should have a specific IP address assigned in the 
own descriptor. The other remaining interfaces, should be external CP.
2. Create an NSD (NSD_1) that includes the VNFD and exposes its external CP as 
external CP of the NS.
3. Instantiate NSD_1 (assuming that the external networks have DHCP enabled) 
and check that:
  * The VIM networks have been created with the right IP parameters and 
assigned the proper IP addressed to the VM interfaces, according to descriptor.
  * The NS instance works as expected.
4. Create a second NSD (NSD_2) with 2 VNFD_1. One interface of each VNF is 
connected to an internal VL between both. This VL will have DHCP disabled and a 
specific CIDR. The VNF interfaces connected to the internal VL should have 
specific IP addresses assigned in the own NS descriptor.
5. Instantiate NSD_2 and repeat same checks as in step 3.
6. Create a new NSD (NSD_3) based on NSD_2, setting a different CIDR for the 
internal NSD VL. Instantiate and repeat same checks as in step 3.
7. Instantiate NSD_3 but setting the IP addresses of some external CP of the 
NS. Repeat same checks as in step 3.
8. Check that it is not possible to change VNF_1, NS_1 or NS_2 to set IP 
addresses of external interfaces/CP and onboard them successfully in the system.

