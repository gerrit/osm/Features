# Selective support of MAC address selection, preserving infrastructure and 
descriptor coherency #
 
## Proposer ##
- Pablo Montes (Telefonica)
- Antonio Lopez (Telefonica)
- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)
 
## Type ##
**Feature**
 
## Target MDG/TF ##
IM-NBI, SO, RO
 
## Description ##
Many legacy, non cloud-native, VNFs assume that the VIM can provide specific 
MAC addresses to specific interfaces of their VDUs/VMs. 


This feature request aims to provide the same support to model the same 
behaviour that some of the commonest VIMs already provide for some interface 
types (paravirtualized interfaces and, optionally, SR-IOV). 
 
As in the case of IP profiles, a coherency check is mandatory to prevent that 
these MAC addresses created by the VIM can create conflicts between different 
instances of the same NS/VNF or between VNFs interconnected in a NS.
 
In order to guarantee that coherency, MAC addresses can only be set in a 
descriptor where they correspond to VLs or CPs that are considered 
″internal″ to the corresponding VNFD or NSD. Thus:
* At VNFD level, a specific MAC address can be set only in internal 
interfaces/CP.
* At NSD level, same rules as above would apply, but in the context of a NS 
template (now, many ″external″ VNFD’s CPs or VLDs might become 
″internal″ for the NSD).
* If required, parameters passed at the time of instantiation would allow to 
set MAC addresses of external CP of the NS.
 
It must be noted that in those CPs that are not completely handled by the VIM, 
but by the VNFs + SDN controllers (with SR-IOV), this feature might not be 
feasible. 
 
## Demo or definition of done ##
1. Create a VNFD (VNFD_1) with 2 VDUs, each of them with 2 interfaces (besides 
mgmt). One interface of each VDUs is connected to an internal VL between both 
VDUs. This VL will have DHCP enabled and a specific CIDR. The VDU interfaces 
connected to the internal VL should have a specific MAC address assigned in the 
own descriptor. The other remaining interfaces, should be external CP.
2. Create an NSD (NSD_1) that includes the VNFD and exposes its external CP as 
external CP of the NS.
3. Instantiate NSD_1 (assuming that the external networks have DHCP enabled) 
and check that:
  * The MAC addresses are set according to descriptor.
  * The NS instance works as expected.
4. Create a second NSD (NSD_2) with 2 VNFD_1. One interface of each VNF is 
connected to an internal VL between both. This VL will have DHCP enabled and a 
specific CIDR. The VNF interfaces connected to the internal VL should have 
specific MAC addresses assigned in the own NS descriptor.
5. Instantiate NSD_2 and repeat same checks as in step 3.
6. Instantiate NSD_2 but setting the IP addresses of some external CP of the 
NS. Repeat same checks as in step 3.
7. Check that it is not possible to change VNF_1, NS_1 or NS_2 to set MAC 
addresses of external interfaces/CP and onboard them successfully in the system.
