# Allow alternative images for specific VIMs in the descriptor #
 
## Proposer ##
- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)
 
## Type ##
**Feature**
 
## Target MDG/TF ##
IM-NBI, SO, RO
 
## Description ##
In a hybrid multi-VIM deployment with private and public cloud sites, it would 
be advisable to allow a different strategy of image management for both types 
so it is not mandatory to upload images to the public cloud, since:
- Public cloud often has a set of pre-loaded images (e.g. AWS’s AMIs) that 
could facilitate their immediate consumption to users.
- Uploading a big image to public cloud might lead to relevant charges from the 
cloud provider.
 
Those public cloud images often have their own identifiers, checksums, etc., 
different from the ones for an image in public cloud.
 
The request is to extend the IM for VNFD so that:
- The current image reference is interpreted as the default image reference for 
all VIMs
- Allow, as option, to include references to alternative images for specific 
types of VIMs, that would be used for sites of that VIM type instead of the 
default one.
 
## Demo or definition of done ##
- Create a VNFD with a VDU that has a default VM image and a specific image for 
a particular type of VIM (e.g. AWS).
- Create a NSD that includes the VNF above.
- Create instances of the NS in 2 different VIM, one of them of the type 
specified in the VNFD.
- Check that both of them work properly.
