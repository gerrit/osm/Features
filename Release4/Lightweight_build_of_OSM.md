# Lightweight build of OSM #
 
## Proposer ##
- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Pablo Montes (Telefonica)
- Francisco Javier Ramon (Telefonica)
 
## Type ##
**Feature**
 
## Target MDG/TF ##
IM-NBI, all
 
## Description ##
The current installation of OSM requires an amount of resources (4 CPUs, 8 GB 
RAM, 40GB disk) that, although reasonable for production environments, are 
highly inconvenient for more reduced setups, casual testers or demo 
environments.

On the other hand, not all OSM functionalities are required in those 
environments, but a minimal subset of them, that would allow to use the current 
VNF/NS packages and run the commonest operations: onboard a VNF/NS, remove a 
VNF/NS, instantiate a NS, call a NS primitive, delete a running NS instance, 
etc.

No redundancy or other advanced features are expected, and could be selectively 
removed of this build if there are advantages in terms of resource consumption.

This feature requests an alternative build of OSM that:
- Allows to run those common operations
- Preserves OSM's IM and NBI, so VNF/NS packages and OSM_Client are functional
- Works with a reduced build that has a minimal footprint. TARGET: 2 vCPUs, 2 
GB RAM, 20 GB virtual disk.

Ideally this build should be distributed as a VM image, to facilitate the 
setup, and, optionally, might offer a second VM with a pre-installed version of 
the VIM-emulator, to get the system up and running.

## Demo or definition of done ##
Check that an environment like the one described above runs in a laptop with 
the described resource constraints.
