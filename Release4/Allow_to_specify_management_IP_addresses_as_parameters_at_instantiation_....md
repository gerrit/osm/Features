# Allow to specify management IP addresses as parameters at instantiation time #
 
## Proposer ##
- Pablo Montes (Telefonica)
- Antonio Lopez (Telefonica)
- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)
 
## Type ##
**Feature**
 
## Target MDG/TF ##
IM-NBI, SO, RO
 
## Description ##
Having a management interface ready and reachable right after boot is critical 
for driving Day 1 and Day 2 operations. Currently, OSM assumes that the VIM 
assigns an IP address to the VNF from the pool in the management network (the 
VNF can retrieve it via DHCP or metadata service, if available).
 
However, many legacy OSS assume that the VNFs should have a specific IP address 
assigned from a pool of addresses that they manage (or that they can learn from 
other systems). In those cases, OSM should provide a means to be told, at the 
time of instantiation, which management IP address should be used. Please note 
that this address cannot be part of the VNFD or the NSD or, otherwise, several 
instances of the VNF/NS would have the same IP address, leading to collisions, 
etc.
 
As a pre-requisite, OSM should provide a mean to specify parameters at the time 
of instantiation.
 
## Demo or definition of done ##
- Create a NS with 2 VNFs, VNF-a and VNF-b.
- Instantiate the NS in a site with a VIM, letting the VIM select the 
management IP addresses. Check that both VNFs are reachable using the reported 
IP addresses.
- Instantiate the NS again, now setting the IP address of VNF-a to a specific 
IP address (let VNF-b obtain the IP address from the VIM, as above). Then check 
that both VNFs are reachable using the appropriate IP addresses (VNF-a at the 
one requested; VNF-b at the one reported by OSM).
