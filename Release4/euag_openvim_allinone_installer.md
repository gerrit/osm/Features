# Openvim all-in-one installer #

## Proposer ##
**EUAG**

## Type ##
**Feature**

## Target MDG/TF ##
RO

## Description ##
Current openvim installer is only installing openvim SW and is not installing
other external modules that are required to have a fully-functional openvim
setup. These components are:

* A DHCP server, required to provide management IP addresses to VMs, so that
  they could be accessible by the VCA for later configuration
* An OFC controller to be used to create underlay E-Line and E-LAN networks
* An NFS server for image storage

This feature proposes an all-in-one installer, which will give the possibility
to install those additional components required for a complete setup. The
installer will give the option to install those additional components in the
same server where openvim is running, or in a different one, thus letting the
administrator to decide the deployment option that he prefers.

## Demo or definition of done ##
N/A

