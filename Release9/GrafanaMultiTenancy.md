# Multi tenancy in Grafana

## Proposers

Subhankar Pal (subhankar.pal@altran.com)
Atul Agarwal (atul.agarwal@altran.com)

## Type

Feature

## Target MDG/TF

MON, GRAFANA

## Description

Multi tenancy in Grafana shall be implemented in MON module to enable every user in OSM to have a corresponding user in Grafana. This feature aims to provide data privacy to users by creating separate dashboards of every OSM user.

# Demo or definition of done

Design details posted in the pad.
https://osm.etsi.org/pad/p/htInBG6I7H