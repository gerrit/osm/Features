# Centralized VCA for KNFs #

## Proposer ##
- David García

## Type ##
**Feature**

## Target MDG/TF ##
N2VC, LCM (minimal changes)

## Description ##

Make the VCA aware of the added k8sclusters in OSM, rather than bootstrap juju controllers to the k8sclusters

### Implementation

Current workflow:

1. NBI gets a request to add a k8s cluster (osmclient: `osm k8scluster-add`)
2. LCM handles the request
    2.1. Initializes helm-charm in the k8scluster
    2.2. Initializes juju-bundle in the k8scluster
        2.2.1. Use the k8s credentials to bootstrap a juju controller to the k8scluster
        2.2.2. Store the juju controller credentials in mongodb

Proposed workflow:

1. NBI gets a request to add a k8s cluster (osmclient: `osm k8scluster-add`) (no change)
2. LCM handles the request
    2.1. Initializes helm-charm in the k8scluster (no change)
    2.2. Initializes juju-bundle in the k8scluster
        2.2.1. Use the k8s credentials to **add the k8s cluster as another cloud in the VCA**


## Demo or definition of done ##

This feature is considered complete when any k8scluster added to OSM is added as a cloud to the VCA, and the juju-bundles kdus are deployed to the k8scluster successfully.
