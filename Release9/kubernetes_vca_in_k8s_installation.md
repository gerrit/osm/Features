# Kubernetes VCA in K8s installation #

## Proposer ##
- David García

## Type ##
**Feature**

## Target MDG/TF ##
Devops

## Description ##

Move the VCA from LXD to K8s in the Kubernetes installation. This is already done for Charmed installation.

### Implementation

Currently:

1. Install LXD
2. Bootstrap VCA to LXD
3. Install K8s
4. Add K8s cloud to VCA

Proposal:

1. Install LXD
2. Install K8s
3. Bootstrap VCA to K8s
4. Add LXD cloud to VCA


## Demo or definition of done ##

This feature is considered complete when the VCA is in K8s with LXD as an external cloud.