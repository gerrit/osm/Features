# Addition of new Wim Connector DPB  #

## Proposer ##
- Steven Simpson (Lancaster University)
- Paul McCherry (Lancaster University)

## Type ##
**Feature**

## Target MDG/TF ##
NBI, RO

## Description ##
This feature will add a new WIM connector, DPB to OSM. It is configurable and able to use either a
REST or SSH interface although the SSH interface is mainly used for management operations. It has
the ability to optionally make use of bandwidth parameters to create and edit logical networks.

DPB is an extensible network management framework, offering support for the WIM abstraction in the
NFV-MANO data model. The architecture of the system follows a multi-layer design approach.

At the top of the system is the aggregator layer, which is responsible for service persistence,
allocation of trunk resources, and end-to-end path computation.The aggregator layer exposes a single
northbound interface (NBI) to the resource orchestrator for the management of logical networks
across datacenters.

The aggregator NBI can be used recursively by a top-level aggregator, in order to
abstract different technological or administrative domains and offer scalability through control
delegation.

The logical-switch layer  constitutes the middle layer of the DPB architecture and is
responsible for persisting conﬁguration and forwarding state for network devices. A logical switch
offers the same control NBI as an aggregator. Control of the underlying network devices is achieved
through the fabric adaptation layer, which translates forwarding policy into technology-speciﬁc
conﬁguration.

The communication between the different layers of DPB is facilitated by a hierarchical
data model. DPB offers hierarchical control of programmable WANs and deﬁnes a detailed WAN control
data model. A WIM Connector (wimconn_dpb.py) has been written to integrate DPB with OSM release 6

## Demo or definition of done ##
- See Presentations from Hackfest #7 Patras,
- 1.      DPB Introduction,
- 2.      WIM integration, configuration & Implementation,
- 3.      WIM CDN use case