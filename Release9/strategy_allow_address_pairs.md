# Simple strategy to allow address pairs for virtual IP
 
## Proposers

- Gianpietro Lavado (Whitestack)
- Gerardo García de Blas (Telefonica)

## Type

Feature

## Target MDG/TF

IM, RO

## Description

Openstack VIMs allow to ways to access virtual IPs inside VMs:
1. Per-IP/MAC or prefix, with allow-address-pairs.
2. Full access, by disabling port-security

Today, OSM only supports the second one, which is not supported in some VIMs.
As a different strategy to allow full access in these VIMs that do not support disabling port
security, the proposal is to leverage allowed-address-pairs with a command like this one:

openstack port set --allowed-address ip-address=0.0.0.0/0  PORT_ID

This would not allow full access to the port, but would cover most cases where VIPs need
to be reached.

At the VNFD level, a new entry would need to be added, to support this different strategy of
disabling port security.

## Demo or definition of done

- The VNFD supports a method of disabling port-security, through the use of allowed-address-pairs.
- The ports are implemented with an allowed-address-pair rule that allows 0.0.0.0/0