# Support of Helm version 3

## Proposer
- Alfonso Tierno (Telefonica)
- Gerardo Garcia (Telefonica)
- Francisco Javier Ramon (Telefonica)
- Isabel Garcia (Indra)

## Type
**Feature**

## Target MDG/TF
N2VC, LCM (minimal changes), IM (minimal changes)

## Description

Currently OSM deploys KDUs with juju-bundles and helm-charts. For the helm case, Helm v2 (version 2) is used. Helm v3 was released in November 2019, Helm v2 has reached end of live since August 2020 (November 2020 is the deadline for security patches). 
For this reason OSM should support Helm v3, together with Helm v2 for backward compatibility.


## Demo or definition of done

OSM is able to deploy a helm-chart KDU that needs Helm v3


