# RBAC external integrations readiness & testing

## Proposer

- José Miguel Guzmán (Whitestack)
- Gianpietro Lavado (Whitestack)

## Type

**Feature**

## Target MDG/TF

NBI, Keystone

## Description

Today, RBAC works with a local user database.
This feature aims to complete its readiness and documentation for integrating with
external authentication systems (LDAP, SSO) through the keystone backend.

## Demo or definition of done

OSM can integrate with LDAP and SSO systems.