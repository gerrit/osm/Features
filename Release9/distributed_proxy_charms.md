# Distributed Proxy Charms #

## Proposer ##
- David García

## Type ##
**Feature**

## Target MDG/TF ##
N2VC
NBI
LCM

## Description ##

Be able to select the (k8s) cloud for (k8s) Proxy charms that should be used in a vim account.

### Implementation

#### Currently:

LCM has these ENV variables provided at OSM deployment time to specify the clouds it needs to use for
Proxy charms and K8s Proxy charms respectively:
- OSMLCM_VCA_CLOUD
- OSMLCM_VCA_K8S_CLOUD

With the current implementation, all proxy charms will go to the cloud set in OSMLCM_VCA_CLOUD, and all
k8s proxy charms will go to the cloud set in OSMLCM_VCA_K8S_CLOUD. These clouds must previously exist
in the VCA, OSM doesn't configure them or add them to the VCA.

#### Proposal:

When adding a VIM account, add several options to select another existing clouds to override the default ones.

```bash
$ osm vim-create --type openstack --name openstack --config '{
    vca_cloud: "mycloud",
    vca_cloud_credential: "mycloud_credential",  # If not provided, same name as the "cloud" will be used
    vca_k8s_cloud: "myk8scloud"
    vca_k8s_cloud_credential: "myk8scloud_credential"  # If not provided, same name as the "cloud" will be used
  }'
```


#### Changes in NBI

Add `cloud`, `cloud_credential`, `k8s_cloud`, and `k8s_cloud_credential` to the validation in NBI.

#### Changes in LCM

Before creating or registering an execution environment, check if `cloud` or `k8s_cloud` exist or not,
and pass that data to N2VC.

#### Changes in N2VC

Use the default clouds for (K8s) Proxy Charms if no `cloud` or `k8s_cloud` is provided for the target VIM.
If the `cloud` or `k8s_cloud` are provided, use those clouds instead.



#### Benefits:
- Add flexibility in terms of distributing Proxy Charms across different clouds
- Be able to allocate clouds in the same VIM in which the VNFs will be deployed
- Decrease the impact if a (k8s) cloud goes down.


## Demo or definition of done ##

This feature is considered completed when an OSM user with permissions to create a vim account is able to
select a specific cloud/k8s cloud of the VCA to have all the proxy charms/k8s proxy charms deployed
to it, instead of using the default clouds.
