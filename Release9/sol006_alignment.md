# SOL006 alignment

## Proposer

- José Miguel Guzmán (Whitestack)
- Gianpietro Lavado (Whitestack)

## Type

**Feature**

## Target MDG/TF

IM and all modules

## Description

OSM up to REL8 mantains its own model for descriptors.
Aligning with SOL006 allows OSM to on-board and orchestrate VNFs and NS described by using 
ETSI-standard descriptors, and replacing the previous OSM specific VNFDs and NSDs. 

Of course, some augments would be needed to ensure no functionality is lost during the migration.
These augments should be documented and sent back to NFV ISG for their potential inclusion 
in future releases of the specifications. 

## Demo or definition of done

ETSI SOL006 standard descriptors can be onboarded in OSM transparently.