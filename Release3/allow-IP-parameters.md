# Allow IP parameters at NSD and instantiation #

## Proposer ##
- Alfonso Tierno (Telefónica)
- Gerardo García (Telefónica)

## Type ##
**Feature**

## Target MDG/TF ##
UI, RO, SO

## Description ##
Current OSM deployments cannot define IP parameters for networks (ranges, DHCP, ) at the VNFD and
NSD. Although this can be configured after deployed it is convenient to allow the deployment with
this parameters already configured at the VIM.

The parameters that can be configured are e.g.: IPv4/IPv6, IP address range, DHCP enable and range,
Gateway, DNS, security group, ...  This parameters affect only the L3 networks, it does not apply
to E-LINE type networks for example.

The specific configuration can be defined at 3 levels:

1. A VNFD can contain parameters for its internal networks.
2. An NSD can override this parameters for each one of its networks.
3. Finally, the end user can override this parameters at instantiation time.

In order to have this feature implemented, the following changes are devised:

- In the UI:
    - The end user can see/edit the information for every network at instantiation time
- In the RO:
    - Database changes to annotate instance-related information.
    - Changes in northbound API to allow that information.
    - Changes in VIM connector to transmit this information.
- In the SO:
    - VNFD and NSD data models should incorporate this information
    - Connection to RO must be modified accordingly to the changes in RO northbound API.
    - Database changes to annotate this information.


## Demo or definition of done ##
The test consists in the deployment of an NS instance, where user-defined network parameters have
been defined. Test definition at VNFD level only, add NSD level definition, and finally add NS
instance level definition.

The test is considered successful if the appropriate network parameters are injected to the VIM.

