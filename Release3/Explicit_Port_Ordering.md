# Explicit Port Ordering Support for VDUs #

## Proposer ##
- Rajesh Velandy (RIFT.io)


## Type ##
**Feature**

## Target MDG/TF ##
SO

## Description ##

Implement the ability to specify ordering of interfaces (both external and internal) so that the VM
is orchestrated with those ordering preserved.

## Demo or definition of done ##
Create a descriptor with the ordering specified for a demo VNF and observe that the VNF interfaces
are coming  up in the order specified.