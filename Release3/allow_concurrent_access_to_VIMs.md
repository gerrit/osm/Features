# Allow concurrent access to VIMs  #

## Proposer ##
- Alfonso Tierno (Telefonica)
- Gerardo Garcia (Telefonica)
- Francisco-Javier Ramon (Telefonica)

## Type ##
**Feature**

## Target MDG/TF ##
RO

## Description ##
Currently, OSM does not support concurrent actions over more than one 
datacenter. That becomes even more problematic when the response time in one of 
VIMs is really long or when there is no response at all (e.g. because of a communication problem), since it affects the operations in the rest of sites as well.

In order to solve this issue, it is required to enable the support of 
asynchronous operations in OSM components.

## Demo or definition of done ##
Check that several deployments over the different datacenters can be done in parallel.

## REQUIREMENTS ##
( Implementation details )
At RO:
- New thread per VIM/credential (in the future a pool of threads to use uppon necessity)
    - anwers asynchronously and operates to VIM using the current vimconnector
    - works both with synchronous and asynchronous VIM operations
    - caches in memory VIM information: VMs, images,... periodically refreshed
- vimconnector changes:
    - Class is created/destroyed every time needed. As before
    - Constructor will receive a new python dict variable called persisten_info={}
        - All modifications over this inside the class, available next time class is created
        - Can be used for store login information, tokens, etc.
        - Only if needed it can be stored at database 
    - New function for checking if VIM address and credentials are ok. "check_credentials()"
        - Return None if ok, or raises an exception on error
        - Remove this cheks from vimconnector constructor
