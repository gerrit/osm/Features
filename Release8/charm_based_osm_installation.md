# Installing OSM to Kubernetes using Charms #

## Proposer(s) ##

Adam Israel (Canonical)
David Garcia (Canonical)
Dominik Fleischmann (Canonical)

## Type ##

Feature

## Target MDG/TF ##

Devops, Other

## Description ##

We propose to extend the current installer to support installing OSM on top of Kubernetes using Juju and charms. 

This would be implemented as a new, optional switch to the installer. The user would be able to use the current '-c' switch to specify that they want the charmed version of OSM to be installed, rather than the default Docker Swarm.

OSM charms are an open source collection of charms, under the Apache 2.0 license, that deploy OSM on top of Juju and Kubernetes.

Each charm uses the Docker image produced by the community, so the user is getting the same OSM code, delivered via an alternative method that is designed to be modeled and operated at scale.

Additionally, we'd like to move these charms under OSM governance, into a new git repository called osm-charms. This repository will contain the charms, interfaces, and assorted scripts related to building, testing, and publishing said charms.

This is similar to how charms live upstream in OpenStack:
https://opendev.org/openstack?q=charm&tab=&sort=recentupdate

Lastly, we'll work with the devops community to integrate charms into Jenkins, allowing it to run through stages 1-4 the same as the Docker Swarm and Kubernetes installer options.

This will allow the charmed installation of OSM to undergo the same rigorous testing as other installer methods do in order to ensure the same quality.

We will then work with the upstream community to improve and extend the current testing, i.e., adding Robot tests and exercising use-cases of interest to commercial users of OSM.


## Demo or definition of done ##

As part of the installation of OSM, the user can pass the -c switch to `install_osm.sh` to override the default behavior. 

A user running the command 'install_osm.sh -c charmed' would install a single-instance version of OSM on top of microk8s via charms.

Additional optional switches would be supported, including:

--ha: install in High Availability mode
--k8s_endpoint: Specify the endpoint of an existing Kubernetes to use instead of microk8s
--bundle: Specify a custom bundle.yaml to use to deploy OSM via charms
--microstack: Install and configure microstack alongside OSM
