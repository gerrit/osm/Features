# Juniper Contrail SDN Controller support #

## Proposer ##
Adam Israel (Canonical)
Arno van Huyssteen (Canonical)
David Garcia (Canonical)
Eduardo Sousa (Canonical)

## Type ##
**Feature**

## Target MDG/TF ##
RO

## Description ##
OSM currently supports FloodLight, ONOS and ODL; these are the most used in 
the open source world. While there is a benefit in having this compatibility 
in OSM, there is a recurring need to support commercial SDN controllers, since 
those will be by far more prevalent in real carrier deployments.

The objective of this proposal is to bring to OSM one of the more widely used 
SDN Controllers in the market. While Juniper Contrail provides a wide array of 
features, the scope of this proposal will be to cover SDN Assist use cases.

## Demo or definition of done ##
* Being able to add Juniper Contrail as a SDN Controller.
* Being able to use Juniper Contrail for SDN Assist use cases.
