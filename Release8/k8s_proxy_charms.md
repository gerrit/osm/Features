# Proxy Charms on K8s #

## Proposer ##
Tytus Kurek (Canonical)
David Garcia (Canonical)
Dominik Fleischmann (Canonical)

## Type ##
**Feature**

## Target MDG/TF ##
IM, LCM, N2VC

## Description ##
Currrently proxy charms are deployed on LXD. This allows us to have these workloads
running without consuming a lot of resources.Nevertheless with OSM starting to be
used for production level deployments new methods of deploying these workloads shall
be considered.

By deploying proxy charms on Kuberentes the deployments will be faster and will ensure
all the orchestration features already available in a K8s cluster. This will be achieved
by adding an additional field in the descriptors stating that this proxy charm will be
deployed on Kubernetes (k8s-proxy-charm). The charm will then be deployed in a Kubernetes
that must have been added previously with `k8scluster-add` and will operate the same way
as a proxy charm in LXD.

In the charm implementation the only thing that should change is the charm series in the
metadata.yaml which will be changed to `series: kubernetes`.

## Definition of done #
Once this feature is implemented the user will be able to deploy proxy charms on
kubernetes by changing the above mentioned charm series and setting the k8s-proxy-charm
field in their descriptors.
