# Install OSM to OpenStack using Ansible #

## Proposer(s) ##

Antonio Marsico (BT)

## Type ##

Feature

## Target MDG/TF ##

Devops, Other

## Description ##

Installing OSM on top of an OpenStack infrastructure may be cumbersome. Networks, Security groups, Flavors and Images are a few example of what you need to setup before being ready to install OSM on OpenStack.

This feature proposes the automatic installation of OSM on top of an OpenStack infrastructure. It is based on Ansible, a well-known automation tool. The Ansible playbook performs the following operations:

* Create an external network and its subnet
* Download the Ubuntu 18.04 image and upload it
* Create a security group to allow SSH and HTTP access to an instance
* Create a VM flavour compatible to OSM
* Deploy an Ubuntu 18.04 instance
* Install OSM when the instance becomes available

All these tasks are performed only if required. This is a feature of Ansible. For instance, if the Ubuntu 18.04 image is already present, the task is skipped.

## Demo or definition of done ##

It is an open question to the community if it worth adding this as an option to the `install_osm.sh` installer or leave it as a standalone feature. 

If the standard invocation of Ansible is used, the following procedure is required.

### Playbook execution

In order to execute the playbook, it is required an OpenStack openrc file. It can be downloaded from the OpenStack web interface Horizon.

After that, it can be loaded with the following command:

`$ . openrc`

Then, all the credentials are loaded in the bash environment. Now it is possible to execute the playbook to configure OpenStack and install OSM:

`$ ansible-playbook site.yml`