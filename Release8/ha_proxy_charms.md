# HA Proxy Charms #

## Proposer ##

- Tytus Kurek (Canonical)
- David Garcia (Canonical)
- Dominik Fleischmann (Canonical)

## Type ##

**Feature**

## Target MDG/TF ##

IM, N2VC, LCM

## Description ##

Some features (8681 and 7657) are assuring that an HA VCA will be available
to OSM. Nontheless to achieve full High Availability in Charm Workloads the Charms
must also be designed to achieve that. 

For this, an additional value will have to be added to the descriptor to state the
number of units of each Charm that should be created. This will require modifications
in IM and LCM.

Furthermore, N2VC will have to be modified to recognize the leader unit of each charm
when executing actions.

Finally to assure that future charms can be written with high availability in mind an
example proxy charm will be provided.

## Demo or definition of done ##

Once this feature is done it will be possible to deploy several units of proxy charms in
a LXD Cluster to assure redundancy and high availability between them. This way if one of
the cluster node fails it won't affect the operations of the workloads. The charms will 
have to be written with high availability support.
