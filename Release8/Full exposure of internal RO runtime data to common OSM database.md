# Full exposure of internal RO runtime data to common OSM database

## Proposer

- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type

**Feature**

## Target MDG/TF

RO, LCM

## Description

Currently all runtime information related to resources is keep in an internal 
database in RO, which is partially replicated in the common database, involving 
a translation of models and storage mechanisms. This is inefficient for several 
reasons:

- RO-related information stored in the common database is not authoritative, 
but a copy of the real authoritative info (information is duplicated).
- Therefore, there is no 100% guarantee that the info in the common databases 
is kept in sync with the actual authoritative database.
- There is a translation in place, to adapt the information to the different 
database paradigms (relational vs. noSQL) and map to to system-level IDs and 
objects.
- Almost all changes at IM trigger a specific development to change the 
relational RO database and to make the appropriate translations. This usually 
requieres extensions in RO´s northbound interface capabilities too.
- Maintaining this duplicate mechanism makes RO development and evolution 
artificially complex with no obvious advantages.

Hence this feature requests the direct use of common OSM services by RO 
(particularly, the common database) so that information is always up to date 
and RO's maintenance is largely simplified, being current legacy RO's NBI and 
translation mechanisms no longer needed.

## Demo or definition of done

Check that all information of state in RO is always available in the common 
NoSQL databes, and that internal RO's MySQL database can be safely removed.
