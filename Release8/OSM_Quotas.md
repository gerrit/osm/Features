# Support for OSM Quotas

## Proposers

- Alfonso Tierno (Telefonica)
- Gerardo Garcia (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type

Feature

## Target MDG/TF

NBI, osmclient, UI

## Description

This feature aims to provide quotas at OSM NBI level. A quota is a limit of packages, vims, repos, instances, etc. 
that can be created in OSM for a concrete project. Each project will have each own set of individual quotas. 
Quotas can be set for `nfpackages`, `nspackages`, `netslice-templates`, `netwok-services`, `netlice-instances`, 
`vims`, `wims`, `sdn-controllers`, `pdus`, `k8s-clusters`, `k8s-repos`, `osm-repos`; and in general for any 
other item inside the scope of an OSM project.

Creation of `users`, `projects`, `roles`, etc. will not have quotas, as these are administrative tasks.

Without this quota limit, OSM can be easily attacked, exhausting the database and volume used to store 
packages, just by onboarding plenty of packages in a loop.

A large quota number (e.g. 500) is applied by default when there is not quota established.

## Demo or definition of done

Create a new project. Set a low quota for e.g. nfpackages. Onboard vnfpkgs and test that it is denied when quota limit is reached. A clear error message from NBI should be obtained. Edit project increasing the quota and test that a new vnfpkg can be added. Test that the quota of one project does not affect other projects.

