# VNF Indicator collection using Prometheus exporters
 
## Proposers

- Gianpietro Lavado (Whitestack)
- Gerardo Garcia (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type

Feature

## Target MDG/TF

SA

## Description

VNF indicators are gathered today using Juju Metrics, which MON collects through N2VC.
Juju metrics present some limitations:
- Collection interval is fixed to 5m
- Metrics can only be collected through CLI

The original proposal to replace this with regular, periodic primitives, has been discarded as
primitives are 'blocking', so a metric collection primitive could delay a configuration primitive.

A mechanism is needed to collect indicators through SNMP, REST API and other methods, ideally:
- Adding 'exporters' to store metrics directly to the existing Prometheus TSDB.
- Trying to mantain alignment with current ETSI standards (SOL002, SOL006)

## Demo or definition of done

VNF Indicator collection through SNMP (ant other methods) can be specified at the VNFD, with a 
variable collection period, which will make the system enable the corresponding exporter and 
show the metric at a Network Service dashboard in Grafana.