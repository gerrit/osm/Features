# VNF Repositories

## Proposer
-   José Miguel Guzmán (Whitestack)
-   Gianpietro Lavado (Whitestack)

## Type

Feature

## Target MDG/TF

osmcient

## Description

This feature is for implementing the option to add a VNF package to the OSM
Catalog, from a remote repository.

This includes
- Designing the structure of repositories of VNF Packages and Images, to be consumed by OSM
- The tools for uploading the VNF Packages and Images to a repository
- The option in osmclient to add a VNF package from a remote repository, in the OSM catalog

## Demo or definition of done

VNF package available in a remote repository added to the OSM Catalog, by using
command-line.

Including uploading the images in the VIM

