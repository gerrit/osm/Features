# Replacing OSMLight Weight UI #

## Proposer ##
Prakash Moorthy (prakash.m@tataelxsi.co.in)
Rajesh S (rajesh.s@tataelxsi.co.in)

## Type ##
** New UI  with Single Page Application using Angular 7 for OSM Light Weight UI **

## Target MDG/TF ##
UI

## Description ##
Currently the UI is split across multiple entities - OSM Light Weight UI, VIM UI, using tools like
Graphana for visualizing Prometheus data etc. There is no single UI framework to view all the
relevant information and data which is also extensible to add data and services from new systems.

Proposing the Single Page Application(SPA) using Angular MVC framework to replace OSM Light Weight
UI.
Using modern web platform capabilities we can deliver app-like experiences and can  integrate all
the third party Services with in the same application (Any vim like Open stack, monitoring like
prometheus).
This UI will have the ability to be extensible to add services of any new system in the future
as well.

Advantage of having  MVC approach in OSM Light Weight UI:

Bringing in current standardized technology like Angular is going to enable more people to
contribute
Client and Server modules can be handled independently
MVC renders test-ability, maintainability and scalability. It also satisfies Single Responsibility
Principle of SOLID.
Segregate each part of the program and test that the individual modules by unit testing
Faster development process
Ability to provide multiple views
Support for asynchronous technique
Modification does not affect the entire model

Impact:
Entire Python OSM Light Weight UI code can be replaced using Angular MVC framework without any
worry of regression. The new UI development will happen in a separate repo. Current UI can be
switched when ready without having any impact on running OSM development work (since UI is
dependent on the NBI APIs only)

Features:
Over and above the existing features, we will also bring in first support for OpenStack. Since the
framework is extensible, we can also bring in support for other VIMs as we go along.
Metrics from Prometheus
World class UX design for a elegant B2B experience

## Demo or definition of done ##
Testing all the existing use cases and flows supported by current UI
