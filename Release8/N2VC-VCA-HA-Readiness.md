## Type

Feature

## Target MDG/TF

LCM

## Description

LCM readiness to be able to work with a VCA (Juju Controller) in HA mode.

## Demo or definition of done

If configuration of clustering is transparent to LCM (depending on the final result
of the implementation)

- Use juju enable-ha to create additional instances of controllers

- Stop the original controller instance and check that operation (NS instantiation
and day-2 operation) is unaffected

If configuration is not transparent, install a Juju controller in ha mode and
make the LCM of a fresh OSM installation point to it, with the appropriate 
configuration (e.g. three IP addresses or whatever other mechanism is finally defined), 
and check that operation (NS instantiation and day-2 operation) is unaffected