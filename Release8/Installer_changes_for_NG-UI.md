# Installer changes for NG-UI

## Proposers

VIJAY NAG B S (vijaynag.bs@tataelxsi.co.in)

## Type

Feature

## Target MDG/TF

devops

## Description

Installer shall have an option (-n) to install OSM with NG-UI. Default installation of OSM
shall be with light-ui.

./install_osm.sh -n ngui shall install osm with NG-UI. The changes shall be implemented for both
docker swarm and kubernetes installation.

# Demo or definition of done

To install OSM with with NG-UI -n ngui shall be used. 

For docker swarm a new docker-compose-ngui.yaml file is implemented to  deploy OSM with NGUI and for
k8s new yaml file is implemented.

A flag is set for ngui option, and it is checked before copying docker-compose from $OSM_DEVOPS to 
$OSM_DOCKER_WORK_DIR and OSM_K8S_WORK_DIR respectively.

https://osm.etsi.org/gerrit/#/c/osm/devops/+/8735/
