# Cleanup old code in full_install_osm.sh #

## Proposer ##
VIJAY NAG B S (vijaynag.bs@tataelxsi.co.in)

## Type ##
**Feature**

## Target MDG/TF ##
DevOps

## Description ##
Full Installer script(full_install_osm.sh) has code residues from old releases.
This feature aims to cleanup unused code.

## Demo or definition of done ##