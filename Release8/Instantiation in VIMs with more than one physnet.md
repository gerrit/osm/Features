# Instantiation in VIMs with more than one physnet

## Proposer

- Gerardo Garcia (Telefonica)
- Alfonso Tierno (Telefonica)
- Francisco Javier Ramon (Telefonica)

## Type

**Feature**

## Target MDG/TF

RO, CLI (optional)

## Description

In environments where some physical redundancy is required in terms of 
networking, it is common the use of schemas with more than one switch upstream, 
dividing the physical medium in groups of physical interfaces depending of the 
upstream switch they are attached. In order to facilitate a sensible management 
of these physical interfaces belonging to different "redundancy groups" by the 
VIM, there is the possibility to classify them into the so-called 'physnets', 
so that the VIM can leverage on that physical redundacy if needed.

While this feature should not create a fundamental change in OSM operation or 
the way its modelling works, and it is a fact that OSM can work with these 
environments, it is also true that it works today with some limitations when 
SDN Assist is in place. Thus, in a VIM with multiple physnets, only one can be 
registered today when configuring a VIM target in OSM, leading to potential 
underuse of resources (usually, by one half). Furthermore, when those physnets 
obbey to some kind of physical active-active scheme, OSM cannot leverage on 
this information to make NS/NSI deployments more reliable.

This feature intends to solve the limitations described above.

## Demo or definition of done

With a VIM with multiple physnets, check that it is possible:

- Register the VIM with all its physnets when defining a VIM target in OSM with 
SDN Assist.
- Deploy a large NS requiring SDN Assist in such a VIM so that no interfaces 
are excluded because of the physnet they belong.
