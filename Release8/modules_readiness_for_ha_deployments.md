# Modules readiness for HA deployments #

## Proposer ##
- Benjamin Diaz (Whitestack)
- Gianpietro Lavado (Whitestack)

## Type ##
**Feature**

## Target MDG/TF ##
LCM,MON,POL,RO,N2VC/VCA,common

## Description ##
The goal of this feature is to work on the readiness of each module for them to be ready for high-availability deployments, independently from the platform used to provide HA (Kubernetes, Docker Swarm, and so on)

To get started, two main changes are needed:
1. Dockerized modules that are Kafka consumers should add their group-id when registering, leveraging the work related to Bug534 (https://osm.etsi.org/bugzilla/show_bug.cgi?id=534)
2. Juju HA cluster must be tested according to https://docs.jujucharms.com/2.3/en/controllers-ha

Besides those two actions, other work might be required at some modules for multiple replicas to work in a synchronized way (to be investigated)

Once this is ready and maintained, future efforts should include upstreaming multiple types of HA installations that leverage this work.

## Demo or definition of done ##
A multi-host, HA installation of OSM (independently on how it was achieved) should work without modifications to the modules.