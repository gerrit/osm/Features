# Adding Placement to OSM #

## Proposer ##

- Patrik Rynbäck (Arctos Labs)
- Lars-Göran Magnusson (Arctos Labs)
- Mats Eriksson (Arctos Labs)

## Type ##
*Feature*

## Target MDG/TF ##
IM, NBI, RO, LCM, PLA (possibly a new module in OSM)

## Description ##
Add placement capability, denoted PLA, that supports computation of optimal placement of VNFs over VIMs by matching NS specific requirements to infrastructure availability and run-time metrics, while considering cost of compute/network.

Placement is an optional functionality used at NS instantiation and is invoked by specifying additional parameters to the config field of the CLI ns-create command, or via corresponding form in a GUI (e.g. LW-GUI). The config attribute 'additionalParametersForNS' shall contain 'placementEngine:PLA' when placement support is requested.

The NSD shall contain descriptions of constraints for selected characteristics that needs to be satisfied in order for a specific placement to be valid. The proposal is to initially (OSM Release SEVEN) only support link latency and jitter as constraints on the vld:s. For this it is necessary to make additions to nsd-base.yang and mano-types.yang. These additions will impact IM. These additions does not interfere with existing functionality.


Placement optimization is triggered by LCM. LCM shall upon reception of 'Instantiate NS' with placement request send a message, 'get_placement()', over the Kafka bus to PLA. PLA will in turn compute the optimal placement result and return information on which VNFs that should go on which VIM. LCM will, upon reception of the PLA result response message, update NSR and VNFR database information and continue with instantiate processing.

(Note: In the PLA prototype realization, as demonstrated at OSM-MR#7 in Patras, NBI interacts with PLA to update the 'Instantiate NS' message and dispatch the updated message, including config attributes with VIM placement, to LCM.)

PLA needs a network topology model describing the VIM:s and their interconnections. In addition to network topology PLA also need other input to its computation. Most notably is the latency and jitter metrics. For OSM release SEVEN latency and jitter metrics are provided in configuration files which are read by PLA at each invocation from LCM. In due time, the file based metrics should be replaced by real-time metrics collected via MON plugin.

Finally there is also a need for price lists for compute and communication links. How this in the end should be provided to PLA is for further discussion. For OSM Release SEVEN, price lists, is in the same way as other PLA input, is kept in configuration files with necessary associations to topology data.

At the core of the PLA module is a constraints model built using the Minizinc language, utilizing the associated constraints solver engines. Minizinc is distributed under the Mozilla Public License 2.0. The initial analysis is that there are no licensing implications on OSM with this setup. However, there is a need for a more thorough analysis being conducted by Arctos Labs.

Due to the modifications to IM also NBI and RO are impacted and needs to be rebuilt with updated IM but no changes to NBI or RO functionality is foreseen.

The capability of PLA in OSM Release SEVEN is basic, limited to cost optimized initial placement satisfying latency and jitter requirements expressed in the NSD. These capabilities should be perceived as a baseline for evolution in coming releases after collection of additional goals related to placement optimization.

We suggest an install option to be added so PLA may be included or excluded at OSM installation time.


This proposal has been presented and discussed;
- at OSM MR#7 in Patras. Link to OSM MR#7 presentation; [Placement Module in OSM](https://docbox.etsi.org/OSG/OSM/05-CONTRIBUTIONS/2019/OSM(19)000074_Placement_module_in_OSM.pptx)
- in OSM TECH #186. Link to OSM TECH presentation; [Placement module in OSM - supplement to feature 7953](https://docbox.etsi.org/OSG/OSM/05-CONTRIBUTIONS/2019//OSM(19)000090_Placement_module_in_OSM_-_supplement_to_feature_7953.pptx)

## Demo or definition of done ##
Cost optimized placement of VNFs over different VIMs
- It is possible to create and onboard NS descriptors with placement constraints.
- It is possible to invoke NS instantiation with placement functionality by adding corresponding settings in the ns create operation both in the CLI and GUI.
- It is still possible to invoke NS instantiation without placement support.
- Confirm that placement is achieved by looking at the VNF allocation to VIMs.
- Verify that placement is valid considering available VIMs and established VIM interconnections while satisfying the NS requirements.
- Manually verify that placement is cost optimal by comparing the outcome with other valid placement alternatives (i.e. other placement alternatives that satisfies the constraints).
