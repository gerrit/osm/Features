# Enhancing NG-UI to enable Juju operational view dashboard

## Proposers
Barath Kumar
Vijay R S

## Type

Feature

## Target MDG/TF

NG-UI, NBI, LCM, N2VC

## Description

Juju playes a vital role in configuring Network Functions in OSM. Juju applies its own architectural methods like, models, apps, units etc.,
to deploy charms and manages it. If any user interested to view the operational components of juju the user is expected to use the CLI and
has a way of mapping a model to NS. 
This functionality can be exposed to GUI to increase readability and since juju being key component of OSM exposing it to GUI enables better
user experience. 

NBI shall be levegared as single NB and newer OSM specific APIs can be designed and modelled in the NBI. N2VC being the VCA client utilized
to read the juju specific information and parse it visualise in the GUI.

Functionalities planned to expose to NG-UI,
1) model view of all the deployed models
2) Apps, units, External consumed endpoint lists in a model(equates to an NS)
3) Allow to execute Day-2 operations in the Operational view dashboard.

## Demo or definition of done

NG-UI able to show the Juju Operational view dashboard of launched NS.