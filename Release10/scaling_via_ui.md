# Scaling via UI

## Proposers

Tolga Gölelçin (Telenity)

## Type

Feature

## Target MDG/TF

LW-UI

## Description

Manual scale-in/scale-out commands can be only triggered from CLI. 
When they are triggered, they can be tracked in "History of operations" page. 
So it will be nice if scaling can be triggered also via a link under Actions list box in "NS Instances" page

## Demo or definition of done

Go into "NS Instances" page. Click "Actions" of the NS Instance row. 
From the list select "Manual Scaling" link. 
Then a new pop-up page will opened. 
In this page there will will be a list box which is filled with the all scaling-group-descriptor names captured from all VNFDs in the NSD and whoose scaling-type is "manual". 
An there may be an radio option to switch between scale in / scale out. 
And a "Run" button to trigger scaling action. 
When the scaling-group-descriptor name and the scaling direction are selected the Run button is clicked.
The pop-up window is closed and the page is directed to "History of operations" page for this NS.